# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'parallax.py' is part of sprites2.0source
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
Parallax demo, showing far away and near parallax effects.
"""
from __future__ import print_function, division

import logging
import random
from enum import Enum
from functools import cache

import pygame

from prototypes.prototype1.sprites2 import StaticSprite, SpritesDrawer, Camera, AbstractEntityDrawData, EntitySprite

logging.basicConfig()

logger = logging.getLogger(__name__)

SCREENSIZE = 800, 600


def generate_grass(size, color):
    col = pygame.Color(color)
    ch, s, v, a = col.hsva
    surf = pygame.Surface(size, pygame.SRCALPHA)
    surf.fill((0, 0, 0, 0))  # black transparent
    for i in range(size[0]):
        if random.random() < 0.4:
            continue  # skip
        h = int(random.random() * size[1])
        rh = int(ch + ((random.random() - 0.5) * 15)) % 360
        rv = int(v + ((random.random() - 0.5) * 15)) % 100
        pygame.draw.line(surf, pygame.Color.from_hsva((rh, s, rv, a)), (i, size[1]), (i, size[1] - h),
                         random.randint(1, 3))
    return surf


def generate_mounten(size, color):
    h, s, v, a = color.hsva
    surf = pygame.Surface(size, pygame.SRCALPHA)
    surf.fill((0, 0, 0, 0))  # black transparent
    var_h = random.random() * 0.25 * size[1]
    var_m = (2 * random.random() - 1) * 0.3 * size[0]
    rv = int(v + ((random.random() - 0.5) * 15)) % 100
    col = pygame.Color.from_hsva((h, s, rv, a))
    pygame.draw.polygon(surf, col, ((0, size[1]), size, (size[0] / 2 + var_m, var_h)))
    return surf


class AnimState(Enum):
    Idle = 0
    Walk = 1


class Entity(AbstractEntityDrawData):

    def __init__(self, position: pygame.Vector2, hit_box: pygame.Rect, rotation_degrees=0, layer=0):

        super().__init__(position, rotation_degrees=rotation_degrees)
        self.hit_box: pygame.Rect = hit_box
        self.speed = 100

    def move(self, move_direction: int, dt: float):
        move_distance = move_direction * self.speed * dt
        self.position.x += move_distance
        self.anim_state = AnimState.Walk
        if move_direction < 0:
            self.flip_x = True  # WalkDirection.left
        elif move_direction > 0:
            self.flip_x = False  # WalkDirection.right
        else:
            self.anim_state = AnimState.Idle


@cache
def load_image(file_name):
    return pygame.image.load(file_name).convert_alpha()


@cache
def load_images(file_name, size, count):
    img = load_image(file_name)
    r = pygame.Rect((0, 0), size)
    images = []
    for s in count:
        i = int(s)
        surf = pygame.Surface(r.size, pygame.SRCALPHA)
        surf.fill((0, 0, 0, 0))
        surf.blit(img, (0, 0), r.move((i * r.w, 0)))
        images.append(surf)
    return images


def main():
    pygame.init()
    pygame.display.set_caption("parallax scrolling demo, keys: left, right, n, space, r")
    screen = pygame.display.set_mode(SCREENSIZE)
    sprites = []
    ground_y = 0
    for i in range(100):
        mnt_img = generate_mounten((300, 150), pygame.Color("gray40"))
        spr = StaticSprite(pygame.Vector2(i * 100, ground_y), [mnt_img], layer=-10, offset=pygame.Vector2(0, 75),
                           parallax=pygame.Vector2(0.2, 1.0))
        sprites.append(spr)

        mnt_img = generate_mounten((200, 90), pygame.Color("gray70"))
        spr = StaticSprite(pygame.Vector2(i * 100, ground_y), [mnt_img], layer=-5, offset=pygame.Vector2(0, 45),
                           parallax=pygame.Vector2(0.5, 1.0))
        sprites.append(spr)

        img = generate_grass((40, 30), pygame.Color("darkolivegreen4"))
        spr = StaticSprite(pygame.Vector2(i * 50, ground_y), [img], layer=-1, offset=pygame.Vector2(0, 15),
                           parallax=pygame.Vector2(0.95, 1.0))
        sprites.append(spr)

        img = generate_grass((50, 20), pygame.Color("yellowgreen"))
        spr = StaticSprite(pygame.Vector2(i * 50, ground_y), [img], layer=2, offset=pygame.Vector2(0, 10),
                           parallax=pygame.Vector2(1.05, 1.0))
        # spr.is_lit = False
        sprites.append(spr)

    player = Entity(pygame.Vector2(1500, ground_y), pygame.Rect(0, 0, 30, 90))
    idle_images = load_images("../resources/dude/DudeLookingUp.png", (28, 49), "01234")
    idle_anim = EntitySprite(player, idle_images, fps=10, offset=pygame.Vector2(0.0, 25.0), anim_state=AnimState.Idle)
    walk_images = load_images("../resources/dude/DudeWalking.png", (28, 49), "01234")
    walk_anim = EntitySprite(player, walk_images, fps=10, offset=pygame.Vector2(0.0, 25.0), anim_state=AnimState.Walk)

    sprites.append(idle_anim)
    sprites.append(walk_anim)

    # use the is_hud attribute to draw the following relative to screen (no cam transformation)
    heaven_img = pygame.Surface((800, 400))
    heaven_img.fill(pygame.Color("azure"))
    heaven_spr = StaticSprite(pygame.Vector2(400, 200), [heaven_img], is_hud=True, layer=-1000)
    ground_img = pygame.Surface((800, 200))
    ground_img.fill(pygame.Color("brown"))
    ground_spr = StaticSprite(pygame.Vector2(400, 500), [ground_img], is_hud=True, layer=-1000)
    # ground_spr.is_lit = False
    sprites.append(heaven_spr)
    sprites.append(ground_spr)

    light_img = pygame.Surface((100, 100), pygame.SRCALPHA)
    light_img.fill((0, 0, 0, 0))
    radius = 50
    pygame.draw.circle(light_img, (250, 250, 250, 255), (radius, radius), radius)
    light_img.fill((255, 255, 255, 255), None, pygame.BLEND_RGBA_MULT)
    light_spr = EntitySprite(player, [light_img], anim_state=True)
    light_spr = StaticSprite(player.position, [light_img])
    effect_img = light_img.copy()
    effect_img.fill((150, 150, 150), None, pygame.BLEND_RGB_SUB)
    light_spr.effect = effect_img
    light_spr.is_light = True
    light_spr.special_flags = pygame.BLEND_RGBA_ADD
    light_spr.layer = -0.5
    light_spr.entity.visible = False
    sprites.append(light_spr)

    light_spr2 = StaticSprite(pygame.Vector2(1450, ground_y), [light_img])
    light_spr2.is_light = True
    light_spr2.effect = effect_img
    light_spr2.special_flags = pygame.BLEND_RGBA_ADD
    light_spr2.layer = 2000
    light_spr2.visible = False
    sprites.append(light_spr2)

    sprites_drawer = SpritesDrawer()  # ambient_color=(50, 50, 50, 255))
    cam = Camera(world_position=player.position, follow=player)
    cam.offset.y = 100  # moves the ground line 100px down

    clock = pygame.Clock()
    running = True
    while running:
        dt = clock.tick(60) / 1000.0
        # events
        for event in pygame.event.get():

            if event.type == pygame.QUIT:
                running = False
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    running = False
                elif event.key == pygame.K_SPACE:
                    if cam.zoom == 1.0:
                        cam.zoom = 2.0
                    else:
                        cam.zoom = 1.0
                elif event.key == pygame.K_n:
                    if light_spr.entity.visible:
                        # set to day
                        light_spr.entity.visible = False
                        light_spr2.entity.visible = False
                        sprites_drawer.ambient_color = None
                    else:
                        # set to night
                        light_spr.entity.visible = True
                        light_spr2.entity.visible = True
                        sprites_drawer.ambient_color = (60, 60, 60, 255)
                elif event.key == pygame.K_r:
                    if cam.rotation == 0.0:
                        cam.rotation = 30.0
                    else:
                        cam.rotation = 0.0

        # update
        pressed = pygame.key.get_pressed()
        r_pressed = pressed[pygame.K_RIGHT]
        l_pressed = pressed[pygame.K_LEFT]
        player.move((r_pressed - l_pressed), dt)
        cam.update(dt)

        # draw
        sprites_drawer.render_sprites(screen, sprites, dt, cam)

        pygame.display.flip()

    pygame.quit()


if __name__ == '__main__':
    main()

# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'viewports.py' is part of sprites2.0source
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
Demo to demonstrate the camera capabilities.


"""
from __future__ import print_function, division

import itertools
import math
import random
from functools import cache

import pygame

from sprites3 import Camera, SpriteRenderer, StaticSprite, EntitySprite, AbstractDrawData

SCREENSIZE = 800, 600


def randomize_ship(ship):
    ship.direction = pygame.Vector2(1, 0).rotate(random.randint(0, 360))
    ship.omega = random.choice([-1, 1]) * random.randint(5, 15)
    ship.speed = random.randint(20, 30)


class Ship(AbstractDrawData):

    def __init__(self, world_position):
        super().__init__(world_position)
        self.direction = pygame.Vector2(1, 0).rotate(random.randint(0, 360))
        self.omega = random.choice([-1, 1]) * random.randint(5, 15)
        self.speed = random.randint(20, 30)

    def update(self, dt):
        self.direction.rotate_ip(self.omega * dt)
        self.rotation = math.degrees(math.atan2(self.direction.y, self.direction.x))
        self.position += self.direction * self.speed * dt
        if self.position.x > SCREENSIZE[0]:
            self.position.x = 0
        elif self.position.x < 0:
            self.position.x = SCREENSIZE[0]
        if self.position.y > SCREENSIZE[1]:
            self.position.y = 0
        elif self.position.y < 0:
            self.position.y = SCREENSIZE[1]


@cache
def load_spaceship_image(i):
    source_img = load_image("../resources/spaceship/Player_Ship2c.png")
    r = pygame.Rect(0, 0, 72, 72)
    img = pygame.Surface((72, 72), pygame.SRCALPHA)
    img.blit(source_img, (0, 0), r.move(i * 72, 0))
    return pygame.transform.rotate(img, -90)


@cache
def load_image(p):
    return pygame.image.load(p).convert_alpha()


def main():
    pygame.init()
    pygame.display.set_caption("viewports, scrolling and zooming of cameras, keys: r")
    screen = pygame.display.set_mode(SCREENSIZE)

    viewport_left = pygame.Rect(0, 0, 400, 600).inflate(-10, -10)
    viewport_right = pygame.Rect(400, 0, 400, 600).inflate(-10, -10)
    viewport_minimap_border = pygame.Rect(300, 0, 200, 150)
    viewport_minimap = pygame.Rect(300, 0, 200, 150).inflate(-10, -10)

    ship1 = Ship(pygame.Vector2(200, 300))
    cam_left = Camera(ship1.position, viewport_left, follow=ship1, jump_to=True)

    ship2 = Ship(pygame.Vector2(600, 300))
    cam_right = Camera(ship2.position, view_port=viewport_right, zoom=2.0, follow=ship2)

    cam_mini_map = Camera(pygame.Vector2(400, 300), viewport_minimap, zoom=0.25)
    back_ground_img = pygame.image.load("../resources/background/background.png").convert()
    back_ground_size = back_ground_img.get_size()
    back_spr = StaticSprite(world_position=pygame.Vector2(400, 300), images=[back_ground_img], layer=-1)
    back_spr_top = StaticSprite(world_position=pygame.Vector2(400, 300 - back_ground_size[1]), images=[back_ground_img],
                                layer=-1,
                                flip_y=True)
    back_spr_bottom = StaticSprite(world_position=pygame.Vector2(400, 300 + back_ground_size[1]),
                                   images=[back_ground_img], layer=-1,
                                   flip_y=True)
    back_spr_left = StaticSprite(world_position=pygame.Vector2(400 - back_ground_size[0], 300),
                                 images=[back_ground_img], layer=-1,
                                 flip_x=True)
    back_spr_right = StaticSprite(pygame.Vector2(400 + back_ground_size[0], 300), [back_ground_img], layer=-1,
                                  flip_x=True)

    ship_images = [load_spaceship_image(int(i)) for i in "012321"]
    ship1_spr = EntitySprite(entity=ship1, images=ship_images, fps=10)
    # ship1_spr.precision_rotation = 0.1
    ship2_spr = EntitySprite(entity=ship2, images=ship_images, fps=10)
    # ship2_spr.precision_rotation = 0.2  # -> step size 5

    entities = [ship1, ship2]
    cams = (cam_left, cam_right, cam_mini_map)

    sprites_drawer = SpriteRenderer(fps=60)
    sprites_drawer.limit_fps = False
    sprites = [back_spr,
               ship1_spr,
               ship2_spr, back_spr_top, back_spr_bottom, back_spr_left, back_spr_right]
    sprites_drawer.add_sprites(sprites)

    blit_rects = []
    clock = pygame.Clock()
    fps_clock = pygame.Clock()
    fps_accu = 0
    running = True
    while running:
        dt = clock.tick() / 1000.0
        # events
        for event in pygame.event.get():

            if event.type == pygame.QUIT:
                running = False
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    running = False
                elif event.key == pygame.K_r:
                    randomize_ship(ship1)
                    randomize_ship(ship2)

        # update
        for ent in entities:
            ent.update(dt)

        fps_accu += dt
        if fps_accu > 1:  # every second
            print("fps:", int(fps_clock.get_fps() + 0.5), "ups:", int(clock.get_fps() + 0.5),
                  "blit count:", len(blit_rects), "sprites count:", len(sprites_drawer._sprites),
                  # sprites3._get_image.cache_info(),
                  sprites_drawer.cache_info)
            fps_accu -= 1

        # draw
        if sprites_drawer.next_frame(dt, cams):
            background_color = (255, 0, 255)
            screen.fill(background_color)
            blit_rect_left = sprites_drawer.render_sprites(screen, cam_left)
            blit_rect_right = sprites_drawer.render_sprites(screen, cam_right)
            screen.fill(background_color, viewport_minimap_border)  # fill minimap background -> border
            blit_rect_map = sprites_drawer.render_sprites(screen, cam_mini_map)
            sprites_drawer.end_frame()
            fps_clock.tick()
            blit_rects = tuple(itertools.chain(blit_rect_left, blit_rect_right, blit_rect_map))

    pygame.quit()


if __name__ == '__main__':
    main()

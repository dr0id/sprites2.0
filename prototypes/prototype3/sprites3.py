# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'sprites2.py' is part of sprites2.0source repo.
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

from __future__ import division, print_function

import itertools
import logging
import random
import weakref
from abc import ABC
from collections import OrderedDict, namedtuple
from dataclasses import dataclass
from functools import lru_cache
from typing import List, Iterable

import pygame

VECTOR_ONE_ONE = pygame.Vector2(1.0, 1.0)

logger = logging.getLogger(__name__)


class AbstractDrawData(ABC):
    def __init__(self, world_position, rotation_degrees=0.0, alpha=1.0, flip_x=False, flip_y=False, zoom=1.0,
                 visible=True, anim_state=None, anim_speed=1.0, scale=pygame.Vector2(1.0, 1.0), layer=0,
                 offset=pygame.Vector2(0, 0), parallax=pygame.Vector2(1.0, 1.0),
                 flip_x2=False, flip_y2=False, scale2=pygame.Vector2(1.0, 1.0), rotation_degrees2=0.0, zoom2=1.0):
        self.position: pygame.Vector2 = world_position
        self.alpha = alpha
        self.layer = layer
        self.flip_x = flip_x
        self.flip_y = flip_y
        self.scale = scale
        self.rotation: float = rotation_degrees
        self.zoom = zoom
        self.flip_x2 = flip_x2
        self.flip_y2 = flip_y2
        self.scale2 = scale2
        self.rotation2: float = rotation_degrees2
        self.zoom2 = zoom2
        self.visible = visible
        self.offset = offset
        self.parallax = parallax

        self.anim_speed = anim_speed
        self.anim_state = anim_state  # if None always draw, otherwise only if corresponding entity is in same state


@dataclass
class AttributeLocks:
    position: bool = False
    alpha: bool = False
    layer: bool = False  # todo not sure about this
    flip_x: bool = False
    flip_y: bool = False
    scale: bool = False
    rotation: bool = False
    zoom: bool = False
    flip_x2: bool = False
    flip_y2: bool = False
    scale2: bool = False
    rotation2: bool = False
    zoom2: bool = False
    visible: bool = False  # todo not sure about this
    offset: bool = True
    parallax: bool = True
    # lock_anim_speed: bool


@dataclass
class AttributeLocksTrue:
    position: bool = True
    alpha: bool = True
    layer: bool = True  # todo not sure about this
    flip_x: bool = True
    flip_y: bool = True
    scale: bool = True
    rotation: bool = True
    zoom: bool = True
    flip_x2: bool = True
    flip_y2: bool = True
    scale2: bool = True
    rotation2: bool = True
    zoom2: bool = True
    visible: bool = True  # todo not sure about this
    offset: bool = True
    parallax: bool = True
    # lock_anim_speed: bool  # todo add to AttributeLocks?


_valid_offset_names = (
    "topleft", "midtop", "topright", "midright", "bottomright", "midbottom", "bottomleft", "midleft",
    "center"
)


class Sprite(AbstractDrawData):
    has_draw_method = False  # if True then a draw method `def draw(self, screen, cam)` on the sprite is called
    is_light = False  # decide if it should be handles as a light
    is_affected_by_light = True  # if True, lights illuminate this sprite, false sprite as is (fully lit)
    is_hud = 0  # if 1, use screen coordinates, if 2, use viewport coordinates, otherwise use cam transformation
    special_flags = 0  # todo add to AttributeLocks?
    area = None  # todo add to AttributeLocks?
    anim_state = True

    # precisions
    # alpha is in [0,1] -> 0.1 per step => 0.1 * 255 = 25.5 per step
    # e.g. alpha=0.345643 => int(0.345643 * 10)/10 = int(3.45643)/10 = 0.3 => 0.3 * 255 = 76.5
    precision_alpha = 10
    precision_size = 10  # 10 => 0.1 per step
    precision_rotation = 1  # 1 => 1° step, 0.1 => 10° steps, 0.2 => 5° steps, 0.5 => 2° steps

    def __init__(self, images, world_position=None, rotation_degrees=0.0, alpha=1.0, flip_x=False, flip_y=False,
                 zoom=1.0, visible=True, anim_state=None, anim_speed=1.0, scale=pygame.Vector2(1.0, 1.0),
                 parallax=pygame.Vector2(1.0, 1.0), offset=pygame.Vector2(0.0, 0.0), layer=0,
                 fps=0, area=None, special_flags=0, entity=None, attribute_locks=AttributeLocks(), effect_images=None,
                 flip_x2=False, flip_y2=False, scale2=pygame.Vector2(1.0, 1.0), rotation_degrees2=0.0, zoom2=1.0):
        self.is_alive = True  # if set to false the this sprite will be removed from the sprite rendrer
        if __debug__ and not entity:
            mgs = "world position has to be provided if no entity is set"
            assert world_position is not None and isinstance(world_position, pygame.Vector2), mgs
        world_position = world_position or pygame.Vector2(0, 0)

        # animation
        self.fps = fps
        self.frame_time = 1.0 / (fps + 1e-6)
        self.current_idx = 0
        self.accu = 0

        # images
        self.image: pygame.Surface | None = None  # will be set by self.source_images property setter
        self._source_images = []
        self.source_images_len = 0
        self.source_images = images
        if __debug__ and effect_images:
            assert len(effect_images) == len(images), "same number of images and effect_images should be provided"
        self.effect_images = effect_images

        if isinstance(offset, str):
            img_rect = self.image.get_rect()
            if not hasattr(img_rect, offset) or offset not in _valid_offset_names:
                raise Exception(f"unknown offset value '{offset}', valid are: {','.join(_valid_offset_names)}")
            offset = pygame.Vector2(getattr(img_rect, offset)) - pygame.Vector2(img_rect.center)

        super().__init__(world_position, rotation_degrees=rotation_degrees, alpha=alpha, flip_x=flip_x, flip_y=flip_y,
                         zoom=zoom, visible=visible, anim_state=anim_state, anim_speed=anim_speed, scale=scale,
                         layer=layer, offset=offset, parallax=parallax,
                         flip_x2=flip_x2, flip_y2=flip_y2, scale2=scale2, rotation_degrees2=rotation_degrees2,
                         zoom2=zoom2)

        # spr specific
        if __debug__ and entity:
            assert isinstance(entity, AbstractDrawData), "entity should implement AbstractDrawData"
        self.entity = weakref.ref(entity, self._set_dead_by_ref) if entity else entity
        self.attribute_locks: AttributeLocks = attribute_locks
        self._old_layer = layer

        self.area = area
        self.special_flags = special_flags

        # intern
        self.cached_key = None
        self.cached_img = None
        self.cache_skipped_count = random.randint(0, 10)
        self.pos_rect = self.image.get_rect(center=self.position)

    @property
    def source_images(self):
        return list(self._source_images)  # return copy to avoid bugs if the returned list is manipulated

    @source_images.setter
    def source_images(self, images: List[pygame.Surface]):
        assert len(images) >= 1, "at least one image should be provided"
        self._source_images = images
        self.source_images_len = len(images)
        self.current_idx = self.current_idx if self.current_idx < self.source_images_len else 0
        self.image = self._source_images[self.current_idx]

    def draw(self, screen, cam):
        raise NotImplementedError("To override in inherited classes with 'has_draw_method' set to True")

    def _set_dead_by_ref(self, *args):
        self.is_alive = False


class EntitySprite(Sprite):

    def __init__(self, entity, images, anim_state=None, anim_speed=1.0, parallax=pygame.Vector2(1.0, 1.0),
                 offset=pygame.Vector2(0.0, 0.0), fps=0, area=None, special_flags=0, layer=0):
        super().__init__(images, anim_state=anim_state, anim_speed=anim_speed, parallax=parallax, offset=offset,
                         fps=fps, area=area, special_flags=special_flags, entity=entity, layer=layer)


class StaticSprite(Sprite):

    def __init__(self, world_position, images, rotation_degrees=0.0, alpha=1.0, flip_x=False, flip_y=False,
                 zoom=1.0,
                 visible=True, anim_state=None, anim_speed=1.0, scale=pygame.Vector2(1.0, 1.0),
                 parallax=pygame.Vector2(1.0, 1.0), offset=pygame.Vector2(0.0, 0.0), layer=0,
                 fps=0, area=None, special_flags=0):
        super().__init__(images, world_position=world_position, rotation_degrees=rotation_degrees, alpha=alpha,
                         flip_x=flip_x, flip_y=flip_y, zoom=zoom, visible=visible, anim_state=anim_state,
                         anim_speed=anim_speed, scale=scale, parallax=parallax, offset=offset, layer=layer, fps=fps,
                         area=area, special_flags=special_flags, entity=None)


class LightSprite(Sprite):
    is_light = True

    def __init__(self, world_position, mask_images, effect_images=None, layer=0, offset=pygame.Vector2(0, 0),
                 visible=True, entity=None):
        super().__init__(world_position=world_position, images=mask_images, layer=layer,
                         special_flags=pygame.BLEND_RGB_ADD, offset=offset, visible=visible,
                         effect_images=effect_images, entity=entity)


class TextSprite(Sprite):

    def __init__(self, text, font, entity=None, offset=pygame.Vector2(0, 0), layer=0,
                 world_position=pygame.Vector2(0, 0), attribute_locks=None):
        self._font: pygame.font.Font = font
        self._text = None
        attribute_locks = attribute_locks or AttributeLocksTrue(position=False)
        super().__init__(world_position=world_position, images=[pygame.Surface((0, 0))], offset=offset,
                         entity=entity, layer=layer, attribute_locks=attribute_locks)
        self.text = text

    @property
    def text(self):
        return self._text

    @text.setter
    def text(self, value):
        self._text = value
        self.source_images = [self._font.render(self._text, True, pygame.Color("black"))]


class Camera:

    def __init__(self, world_position=None, view_port=None, zoom=1.0, follow=None, jump_to=False, rotation=0.0):
        # rect on screen in screen coordinates, area in which this cam draws
        self.view_port = view_port or pygame.display.get_surface().get_rect()
        self.center = pygame.Vector2(*self.view_port.topleft) + pygame.Vector2(
            *self.view_port.size) / 2  # center in self.screen coordinates

        self.position: pygame.Vector2 = self.center.copy() if world_position is None else world_position.copy()
        self.target_position = self.position.copy()  # position in the world
        self.smooth_radius = 150
        self.speed = 500
        self.offset = pygame.Vector2(0.0, 0.0)  # use this for a shake effect or similar
        self.zoom = zoom
        self.zoom_offset = 0.0
        self.anchor = pygame.Vector2(0.0, 0.0)  # used to set a different parallax point
        self.follow = follow
        self.jump_to = jump_to
        self.rotation = rotation
        self.rotation_offset = 0  # use this for some offset, e.g. shake effect

    def follow(self, entity):
        assert hasattr(entity, 'position'), "target should have a position attribute"
        self.follow = entity

    # Tell the camera to pan to the given position.
    def lookat(self, world_pos, immediate=False):
        self.follow(None)  # do not follow anymore
        self.target_position.update(world_pos)
        if immediate:
            self.position.update(self.target_position)

    def update(self, dt):
        if self.follow:
            self.target_position.update(self.follow.position)
        # arrive smoothly to target
        distance = self.position.distance_to(self.target_position)
        f = distance / self.smooth_radius
        if f > 1:
            f = 1.0
            if self.jump_to:
                self.position.update(self.target_position)
                return
        else:
            f = f
        dt_f = self.speed * dt * f
        self.position.move_towards_ip(self.target_position, dt_f)

    def to_screen(self, world_position, parallax_factors=pygame.Vector2(1.0, 1.0)):
        parallax_offset = parallax_factors - VECTOR_ONE_ONE
        f_ = (world_position - (self.position + self.anchor))
        parallax_offset.x *= f_.x
        parallax_offset.y *= f_.y
        position = world_position + self.offset + parallax_offset
        position_diff = (position - self.position)
        rotation = self.rotation + self.rotation_offset
        if rotation != 0.0:
            position_diff.rotate_ip(rotation)
        return self.center + (self.zoom + self.zoom_offset) * position_diff

    def to_world(self, screen_position):
        if self.zoom == 0:
            self.zoom = 0.0001  # avoid division by 0
        return (screen_position - self.center).rotate(-self.rotation - self.rotation_offset) / (
                self.zoom + self.zoom_offset) + self.position - self.offset


# todo add sprite groups to manage many sprites at once: add/remove/visible/... at once?
# todo mouse picking of sprites -> sprites need to be stored in SpritesDrawer

# todo is that a good limit? !If the cache is too big then it takes longer to check for hit or miss!
# @lru_cache(maxsize=1000)
def _get_image(alpha10x, flip_x, flip_y, rotation, scale_x100x, scale_y100x, img_id, alpha_precision,
               size_precision, rotation_precision, img):
    # print("transform:", alpha10x, flip_x, flip_y, rotation, scale_x100x, scale_y100x, img_id,
    #       alpha_precision, size_precision, rotation_precision,img)
    if alpha10x != alpha_precision:
        img = img.copy()  # todo if no transform is need return original? Or should it return always a copy?
        img.fill((255, 255, 255, alpha10x / alpha_precision * 255), None, pygame.BLEND_RGBA_MULT)
    if flip_x or flip_y:
        img = pygame.transform.flip(img, flip_x, flip_y)
    if scale_x100x != size_precision or scale_y100x != size_precision:
        # todo make transformation methods configurable?
        img = pygame.transform.scale_by(img, (scale_x100x / size_precision, scale_y100x / size_precision))
        # img = pygame.transform.smoothscale_by(img, (_scale_x100x / 100 , scale_y100x / 100))  # this is blurry
    # if rotation or zoom100x != 100.0:
    #     img = pygame.transform.rotozoom(img, -rotation, zoom100x / 100)  # todo how to avoid the minus?
    if rotation:
        img = pygame.transform.rotate(img, -rotation / rotation_precision)  # todo how to avoid the minus?
    return img


@lru_cache(maxsize=10)
def _get_surf(size, flags, convert_alpha):
    surface = pygame.Surface(size, flags)
    if convert_alpha:
        return surface.convert_alpha()
    else:
        return surface.convert()


_CacheInfo = namedtuple("CacheInfo", ["hits", "misses", "maxsize", "currsize"])


def _filter_is_affected_by_light(spr_f):
    return not spr_f.is_affected_by_light


def _filter_is_light(spr_f):
    return spr_f.is_light


class SpriteRenderer:

    def __init__(self, fps=60, ambient_color=None):

        self._dt = 0
        self.limit_fps = True
        self.default_cam = Camera()

        self._fps = 0
        self._frame_time = 0
        self.fps = fps  # calculate and set _fps and _frame_time
        self._accu = self._frame_time  # draw on first call

        self.ambient_color = ambient_color

        self._cache = OrderedDict()
        self._cache_misses = 0
        self._cache_hits = 0
        self._cache_maxsize = 2000000  # todo should this be settable from outside?

        self._sprites: List[Sprite] = []
        self._sprites_set = set()
        self._need_sort = False
        self.always_sort = False

        self._in_frame = 0

    @property
    def cache_info(self):
        return _CacheInfo(self._cache_hits, self._cache_misses, self._cache_maxsize, self._cache.__len__())

    @property
    def fps(self):
        return self._fps

    @fps.setter
    def fps(self, value):
        self._fps = value
        self._frame_time = 1.0 / self._fps

    def add_sprite(self, spr: Sprite):
        assert isinstance(spr, Sprite), "single sprite expected"
        self._sprites_set = set(spr_ for spr_ in self._sprites_set if spr_.is_alive)
        self._sprites_set.add(spr)
        self._sprites = list(self._sprites_set)
        self._need_sort = True

    def add_sprites(self, sprites: List[Sprite]):
        assert all((isinstance(s, Sprite) for s in sprites)), "list of Sprite instances expected"
        self._sprites_set = set(spr_ for spr_ in self._sprites_set if spr_.is_alive)
        self._sprites_set.update(sprites)
        self._sprites = list(self._sprites_set)
        self._need_sort = True

    def remove_sprite(self, spr: Sprite):
        self._sprites_set.discard(spr)
        self._sprites.remove(spr)  # may raise KeyError if spr not present

    @staticmethod
    def _spr_sort_func(spr: Sprite):
        return (spr.layer if spr.entity is None or spr.attribute_locks.layer else spr.entity().layer,
                spr.position.y if spr.entity is None or spr.attribute_locks.position else spr.entity().position.y,
                spr.is_light)

    # todo rename to begin_frame? would be more similar to end_frame...
    def next_frame(self, dt, cams: Iterable[Camera] = tuple()):
        # separate update and draw loop, only draw n times a second
        if self.limit_fps:
            self._accu += dt
            if self._accu <= self._frame_time:
                return False
            cnt = 0
            while self._accu > self._frame_time:
                self._accu -= self._frame_time
                cnt += 1
            self._dt = cnt * self._frame_time  # update animations and cameras with accumulated delta time
        else:
            self._dt = dt

        # update animations once
        for spr in (s for s in self._sprites if s.source_images_len > 1 and s.is_alive):
            # update animation
            spr.accu += self._dt
            anim_speed = spr.entity().anim_speed if spr.entity else spr.anim_speed
            while spr.accu >= spr.frame_time:  # skip frames if dt is longer than a frame
                # todo: if anim_speed is set near 0 this will never recover.... (how to prevent that?)!
                spr.accu -= (spr.frame_time / anim_speed)
                spr.current_idx += 1
                if spr.current_idx >= spr.source_images_len:
                    # todo add optional callback after animation end -> auto remove option?
                    spr.current_idx -= spr.source_images_len
                spr.image = spr._source_images[spr.current_idx]

        # update cameras
        for c in cams:
            c.update(self._dt)
        self.default_cam.update(self._dt)

        if self._in_frame:
            raise Exception("Did you forget to call 'end_frame()'?")
        self._in_frame = 1

        return True

    # todo how to solve multiple renderers? -> flip should only be called once
    def end_frame(self):
        pygame.display.flip()
        self._in_frame = 0
        # todo this method allows to measure frame duration

    def render_sprites(self, screen: pygame.Surface, cam=None):
        # sort if needed
        if self._need_sort or self.always_sort:
            self._sprites[:] = [spr_ for spr_ in self._sprites if spr_.is_alive]
            self._sprites.sort(key=self._spr_sort_func)
            self._need_sort = False
            sorted_sprites = self._sprites
        else:
            sorted_sprites = [spr_ for spr_ in self._sprites if spr_.is_alive]

        # todo remove not visible sprites here already, here?
        # todo remove sprites outside of view area here already, here?
        #      only draw potentially visible sprites -> depends on cam and sprite zoom, scale, etc.!!

        cam = cam or self.default_cam
        orig_clip = screen.get_clip()  # todo implement a clip context manager?
        screen.set_clip(cam.view_port)

        rects = []
        if self.ambient_color:
            self._render_with_light(sorted_sprites, cam, rects, screen)
        else:
            # todo should is_light sprites be filtered out here?
            self._render_sprites(sorted_sprites, cam, rects, screen)

        screen.set_clip(orig_clip)

        return rects

    def _render_with_light(self, sorted_sprites, cam, rects, screen):
        light_groups = []
        sprite_groups = []

        is_first = True
        for is_light_key_, spr_group_ in itertools.groupby(sorted_sprites, key=_filter_is_light):
            if is_light_key_:
                light_groups.append(list(spr_group_))
                if is_first:
                    sprite_groups.append([])
            else:
                sprite_groups.append(list(spr_group_))  # copy to list since underlying iterator is shared
            if is_first:
                is_first = False

        light_groups.append([])
        light_groups.append([])  # depending on sprites groups with/without lights or affected_by_light this is needed

        light_sprites = list(itertools.chain.from_iterable(light_groups))
        light_sprites_remove = light_sprites.remove

        # ambient_surf = pygame.Surface(screen.get_size()).convert()
        ambient_surf = _get_surf(screen.get_size(), 0, False)
        ambient_surf.fill(self.ambient_color)  # fill entire surf to avoid side effects
        cam_view_port = cam.view_port
        ambient_surf.set_clip(cam_view_port)

        # screen_copy = pygame.Surface(screen.get_size(), pygame.SRCALPHA).convert_alpha()  # need the alpha!!
        screen_copy = _get_surf(screen.get_size(), pygame.SRCALPHA, True)  # need the alpha!!
        screen_copy_fill_color = (0, 0, 0, 0)
        pygame_blend_rgba_mult = pygame.BLEND_RGBA_MULT
        screen_copy.fill(screen_copy_fill_color, None, pygame_blend_rgba_mult)  # black transparent surf
        screen_copy.set_clip(cam_view_port)
        screen_copy_fill = screen_copy.fill
        screen_copy_blit = screen_copy.blit
        self__render_sprites = self._render_sprites
        screen_blit = screen.blit
        light_group_first = light_groups[0]

        for spr_gg in sprite_groups:
            for k, g in itertools.groupby(spr_gg, key=_filter_is_affected_by_light):
                if k is True:  # lit
                    self__render_sprites(g, cam, rects, screen)
                    screen_copy_fill(screen_copy_fill_color, cam_view_port, pygame_blend_rgba_mult)  # black transparent
                else:
                    # blit all remaining lights to the ambient_surf (BLEND_RGB_ADD )
                    self__render_sprites(light_sprites, cam, rects, ambient_surf, is_light_mask=True)
                    # blit sprites to transparent screen_copy
                    self__render_sprites(g, cam, rects, screen_copy)
                    # multiply transparent screen_copy with the ambient_surf (make dark)
                    screen_copy_blit(ambient_surf, cam_view_port, cam_view_port, pygame.BLEND_RGB_MULT)
                    # blit darkened screen_copy to screen
                    screen_blit(screen_copy, cam_view_port, cam_view_port, 0)

            # render lights of this layer again to screen (BLEND_RGB_ADD) to add a light effect
            self__render_sprites(light_group_first, cam, rects, screen, is_light_mask=False)

            for l_spr in light_group_first:
                light_sprites_remove(l_spr)
            light_groups.pop(0)
            light_group_first = light_groups[0]

            # prepare for next iteration
            ambient_surf.fill(self.ambient_color)  # reset to ambient color
            screen_copy_fill(screen_copy_fill_color, cam_view_port, pygame_blend_rgba_mult)  # black transparent surf

    def _render_sprites(self, _sorted_sprites: Iterable[Sprite], cam, rects, screen, is_light_mask=False):
    def _render_sprites(self, sorted_sprites: Iterable[Sprite], cam, rects, screen, is_light_mask=False):
        blit_args = []
        blit_args_append = blit_args.append
        blit_sprites = []
        blit_sprites_append = blit_sprites.append

        cam_view_port_colliderect = cam.view_port.colliderect
        cam_to_screen = cam.to_screen

        self_cache = self._cache
        self_cache_get = self_cache.get
        self_cache_move_to_end = self_cache.move_to_end
        cache_size = self._cache_maxsize

        for spr in sorted_sprites:

            if spr.entity is None:
                visible = spr.visible
                if not visible:
                    continue
                spr_position = spr.position
                spr_alpha = spr.alpha
                spr_flip_x = spr.flip_x
                spr_flip_y = spr.flip_y
                spr_scale = spr.scale
                spr_rotation = spr.rotation
                spr_zoom = spr.zoom
                spr_flip_x2 = spr.flip_x2
                spr_flip_y2 = spr.flip_y2
                spr_scale2 = spr.scale2
                spr_rotation2 = spr.rotation2
                spr_zoom2 = spr.zoom2
                spr_offset = spr.offset
                spr_parallax = spr.parallax
                spr_anim_speed = spr.anim_speed
            else:
                # get the data: either from sprite or an entity
                entity = spr.entity()
                locks = spr.attribute_locks  # todo only set if needed, otherwise take the values directly from entity!
                entity_visible = spr.visible if locks.visible else entity.visible
                # if not (entity_visible and (spr.anim_state is None or spr.anim_state == entity.anim_state)):
                if not (entity_visible and (spr.anim_state == entity.anim_state or spr.anim_state is None)):
                    continue
                # check if the attribute is locked, if true fallback to spr value instead of the entity value
                spr_position = spr.position if locks.position else entity.position
                spr_alpha = spr.alpha if locks.alpha else entity.alpha
                spr_flip_x = spr.flip_x if locks.flip_x else entity.flip_x
                spr_flip_y = spr.flip_y if locks.flip_y else entity.flip_y
                spr_scale = spr.scale if locks.scale else entity.scale
                spr_rotation = spr.rotation if locks.rotation else entity.rotation
                spr_zoom = spr.zoom if locks.zoom else entity.zoom
                spr_flip_x2 = spr.flip_x2 if locks.flip_x2 else entity.flip_x2
                spr_flip_y2 = spr.flip_y2 if locks.flip_y2 else entity.flip_y2
                spr_scale2 = spr.scale2 if locks.scale2 else entity.scale2
                spr_rotation2 = spr.rotation2 if locks.rotation2 else entity.rotation2
                spr_zoom2 = spr.zoom2 if locks.zoom2 else entity.zoom2
                spr_offset = spr.offset if locks.offset else entity.offset
                spr_parallax = spr.parallax if locks.parallax else entity.parallax
                spr_anim_speed = entity.anim_speed
            spr_offset = spr_offset.copy()
            if spr_flip_x:
                spr_offset.x = -spr_offset.x
            if spr_flip_y:
                spr_offset.y = -spr_offset.y

            # draw
            # fixme this is broken currently, not sure how to integrate with the blits method -> groups?
            # todo: is this worth it? Why use this?
            if spr.has_draw_method:
                r = spr.draw(screen, cam)
                assert isinstance(r, pygame.Rect), "return of draw method should be dirty rect"
                rects.append(r)
                continue

            # convert to screen position
            if spr.is_hud == 0:
                spr_zoom *= (cam.zoom + cam.zoom_offset)
                spr_rotation += cam.rotation + cam.rotation_offset
                # todo inline cam.to_screen for performance?
                screen_position = cam_to_screen(spr_position, spr_parallax)
            elif spr.is_hud == 1:  # todo rel to screen ?? What about cam.viewport.clip/screen.clip?
                screen_position = spr_position
            elif spr.is_hud == 2:  # todo rel to viewport??
                screen_position = pygame.Vector2(cam.view_port.topleft) + spr_position
            else:
                raise ValueError(f"spr.is_hud value {spr.is_hud} is invalid!")

            scale_x, scale_y = spr_scale.xy
            screen_offset = spr_offset * spr_zoom
            screen_offset.x *= scale_x
            screen_offset.y *= scale_y
            if spr_rotation != 0.0:
                screen_offset.rotate_ip(spr_rotation)
            center = screen_position - screen_offset

            # get image
            # todo subpixel sprites with subpixel surface
            if spr.is_light:
                if is_light_mask:
                    img = spr.image
                elif spr.effect_images:
                    img = spr.effect_images[spr.current_idx]
                else:
                    continue  # no effect to apply
            else:
                img = spr.image

            # spr.pos_rect = img.get_rect(center=center)
            # img_in_view_port = cam_view_port_colliderect(spr.pos_rect)
            # if not img_in_view_port and spr.cache_skipped_count < 10:
            #     spr.cache_skipped_count += 1
            #     continue
            # spr.cache_skipped_count = 0

            # get transformed and cached image
            # key = (img, spr_flip_x, spr_flip_y, spr_rotation, spr_zoom, spr_scale.x, spr_scale.y, spr_alpha)

            # cut to certain precision otherwise each float value would generate a miss in the cache even if the
            # difference is small and visually not noticeable. Event so there will be enough entries to cache!
            # todo would it be faster to have sprite properties that prepare the needed value so the
            #      key calculations could be removed here? Could be also used for a dirty flag.
            #      Thinking about many static sprites that do not change at all. But on the other hand
            #      there could be many sprites that are updated (position, rotation, scale, zoom, alpha, flip, ...)
            scale_x2, scale_y2 = spr_scale2.xy
            alpha_precision = spr.precision_alpha
            size_precision = spr.precision_size
            rotation_precision = spr.precision_rotation

            key = (int(spr_alpha * alpha_precision),  # 0.1 precision -> 255/10 = 25.5 steps
                   spr_flip_x ^ spr_flip_x2,  # bool
                   spr_flip_y ^ spr_flip_y2,  # bool
                   int(((spr_rotation + spr_rotation2) % 360) * rotation_precision),  # 1 degree precision
                   int(spr_zoom * spr_zoom2 * scale_x * scale_x2 * size_precision),  # 0.01 precision  is that enough?
                   int(spr_zoom * spr_zoom2 * scale_y * scale_y2 * size_precision),  # 0.01 precision is that enough?
                   # int(_zoom * 100),  # 0.01 precision is that enough?
                   id(img),
                   alpha_precision,  # need to include; when different values are used then same key can result
                   size_precision,  # need to include; when different values are used then same key can result
                   rotation_precision,  # need to include; when different values are used then same key can result
                   img,)
            # print(key)
            if spr.cached_key == key:
                img = spr.cached_img
                self._cache_hits += 1
            else:
                img = self_cache_get(key, None)
                if img is None:
                    # miss, add new item
                    self._cache_misses += 1
                    img = _get_image(*key)
                    self_cache[key] = img  # appended last, newest at the end
                    if self_cache.__len__() > cache_size:
                        # remove the least used item at beginning
                        self_cache.popitem(last=False)  # pop first
                else:
                    # hit
                    self._cache_hits += 1
                    # move this accessed/added item to the end, this causes least used to be first
                    self_cache_move_to_end(key)

                spr.cached_key = key
                spr.cached_img = img

            # collect blit info
            pos_rect = img.get_rect(center=center)
            if cam_view_port_colliderect(pos_rect):
                # spr.blit_rect = screen.blit(img, pos, spr.area, spr.special_flags)
                blit_args_append((img, pos_rect, spr.area, spr.special_flags))
                blit_sprites_append(spr)
                # rects.append(spr.blit_rect)

        # blit to screen
        blit_rects = screen.blits(blit_args, True)
        for idx, r in enumerate(blit_rects):
            blit_sprites[idx].blit_rect = r  # store the effective rect on screen for picking
        rects.extend(blit_rects)

# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'parallax.py' is part of sprites2.0source
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
Parallax demo, showing far away and near parallax effects.
"""
import random
from enum import Enum
from functools import cache

import pygame

from sprites3 import Sprite, Camera, AbstractDrawData, EntitySprite, StaticSprite, SpriteRenderer, \
    LightSprite

SCREENSIZE = 800, 600


def generate_grass(size, color):
    col = pygame.Color(color)
    ch, s, v, a = col.hsva
    surf = pygame.Surface(size, pygame.SRCALPHA)
    surf.fill((0, 0, 0, 0))  # black transparent
    for i in range(size[0]):
        if random.random() < 0.4:
            continue  # skip
        h = int(random.random() * size[1])
        rh = int(ch + ((random.random() - 0.5) * 15)) % 360
        rv = int(v + ((random.random() - 0.5) * 15)) % 100
        pygame.draw.line(surf, pygame.Color.from_hsva((rh, s, rv, a)), (i, size[1]), (i, size[1] - h),
                         random.randint(1, 3))
    return surf


def generate_mounten(size, color):
    h, s, v, a = color.hsva
    surf = pygame.Surface(size, pygame.SRCALPHA)
    surf.fill((0, 0, 0, 0))  # black transparent
    var_h = random.random() * 0.25 * size[1]
    var_m = (2 * random.random() - 1) * 0.3 * size[0]
    rv = int(v + ((random.random() - 0.5) * 15)) % 100
    col = pygame.Color.from_hsva((h, s, rv, a))
    pygame.draw.polygon(surf, col, ((0, size[1]), size, (size[0] / 2 + var_m, var_h)))
    return surf


class AnimState(Enum):
    Idle = 0
    Walk = 1


class Entity(AbstractDrawData):

    def __init__(self, position: pygame.Vector2, hit_box: pygame.Rect, rotation_degrees=0, layer=0):

        super().__init__(world_position=position, rotation_degrees=rotation_degrees)
        self.hit_box: pygame.Rect = hit_box
        self.speed = 100

    def move(self, move_direction: int, dt: float):
        move_distance = move_direction * self.speed * dt
        self.position.x += move_distance
        self.anim_state = AnimState.Walk
        if move_direction < 0:
            self.flip_x = True  # WalkDirection.left
        elif move_direction > 0:
            self.flip_x = False  # WalkDirection.right
        else:
            self.anim_state = AnimState.Idle


@cache
def load_image(file_name):
    return pygame.image.load(file_name).convert_alpha()


@cache
def load_images(file_name, size, count):
    img = load_image(file_name)
    r = pygame.Rect((0, 0), size)
    images = []
    for s in count:
        i = int(s)
        surf = pygame.Surface(r.size, pygame.SRCALPHA)
        surf.fill((0, 0, 0, 0))
        surf.blit(img, (0, 0), r.move((i * r.w, 0)))
        images.append(surf)
    return images


def main():
    pygame.init()
    pygame.display.set_caption("parallax scrolling demo, keys: left, right, n, space, r")
    screen = pygame.display.set_mode(SCREENSIZE)
    sprites = []
    ground_y = 0
    for i in range(100):
        mnt_img = generate_mounten((300, 150), pygame.Color("gray40"))
        spr = Sprite(world_position=pygame.Vector2(i * 100, ground_y), images=[mnt_img], layer=-10,
                     offset=pygame.Vector2(0, 75), parallax=pygame.Vector2(0.2, 1.0))
        spr.is_affected_by_light = False
        sprites.append(spr)

        mnt_img = generate_mounten((200, 90), pygame.Color("gray70"))
        spr = Sprite(world_position=pygame.Vector2(i * 100, ground_y), images=[mnt_img], layer=-5,
                     offset=pygame.Vector2(0, 45), parallax=pygame.Vector2(0.5, 1.0))
        sprites.append(spr)

        img = generate_grass((40, 30), pygame.Color("darkolivegreen4"))
        spr = Sprite(world_position=pygame.Vector2(i * 50, ground_y), images=[img], layer=-1,
                     offset=pygame.Vector2(0, 15), parallax=pygame.Vector2(0.95, 1.0))
        spr.is_affected_by_light = False
        sprites.append(spr)

        img = generate_grass((50, 20), pygame.Color("yellowgreen"))
        spr = Sprite(world_position=pygame.Vector2(i * 50, ground_y), images=[img], layer=2,
                     offset=pygame.Vector2(0, 10), parallax=pygame.Vector2(1.05, 1.0))
        # spr.is_lit = False
        sprites.append(spr)

    player = Entity(pygame.Vector2(1500, ground_y), pygame.Rect(0, 0, 30, 90))
    idle_images = load_images("../resources/dude/DudeLookingUp.png", (28, 49), "01234")
    idle_anim = EntitySprite(player, idle_images, fps=5, offset=pygame.Vector2(0.0, 25.0), anim_state=AnimState.Idle)
    walk_images = load_images("../resources/dude/DudeWalking.png", (28, 49), "01234")
    walk_anim = EntitySprite(player, walk_images, fps=10, offset=pygame.Vector2(0.0, 25.0), anim_state=AnimState.Walk)

    sprites.append(idle_anim)
    sprites.append(walk_anim)

    # use the is_hud attribute to draw the following relative to screen (no cam transformation)
    heaven_img = pygame.Surface((800, 400))
    heaven_img.fill(pygame.Color("azure"))
    heaven_spr = StaticSprite(pygame.Vector2(400, 200), [heaven_img], layer=-1000)
    heaven_spr.is_hud = 1
    ground_img = pygame.Surface((800, 200))
    ground_img.fill(pygame.Color("brown"))
    ground_spr = StaticSprite(pygame.Vector2(400, 500), [ground_img], layer=-1000)
    ground_spr.is_hud = 1
    # ground_spr.is_lit = False
    sprites.append(heaven_spr)
    sprites.append(ground_spr)

    light_mask = pygame.Surface((100, 100), pygame.SRCALPHA)
    light_mask.fill((0, 0, 0, 0))
    radius = 50
    pygame.draw.circle(light_mask, (150,) * 3, (radius, radius), radius)
    light_mask.fill((255,) * 4, None, pygame.BLEND_RGBA_MULT)
    effect_img = light_mask.copy()
    effect_img.fill((100,) * 3, None, pygame.BLEND_RGB_SUB)
    pygame.draw.circle(effect_img, (100,) * 3, (radius, radius), 25)
    # light_mask.fill((0,)*4, None, pygame.BLEND_RGBA_MULT)
    effect_images = [effect_img]
    # effect_images = None
    light_masks = [light_mask]
    # light_masks = [effect_img]

    # todo hack: using shared position! -> problem: the visible flag of the entity is not the same as for the light!
    # proper way would probably be to have a light entity attached to the player and use a EntitySprite on the light
    # entity!
    light_spr = Sprite(world_position=player.position, images=light_masks)
    light_spr.effect_images = effect_images
    light_spr.is_light = True
    light_spr.special_flags = pygame.BLEND_RGB_ADD
    light_spr.layer = -.5
    light_spr.visible = False
    sprites.append(light_spr)

    light_spr2 = StaticSprite(pygame.Vector2(1450, ground_y), light_masks)
    light_spr2.is_light = True
    light_spr2.effect_images = effect_images
    light_spr2.special_flags = pygame.BLEND_RGB_ADD
    light_spr2.layer = 2000
    light_spr2.visible = False
    sprites.append(light_spr2)

    light_spr3 = LightSprite(world_position=pygame.Vector2(1300, ground_y), mask_images=light_masks,
                             effect_images=effect_images, layer=-3, visible=False)
    sprites.append(light_spr3)

    sprites_drawer = SpriteRenderer(fps=60)  # ambient_color=(50, 50, 50, 255))
    sprites_drawer.limit_fps = False
    sprites_drawer.add_sprites(sprites)
    cam = Camera(world_position=player.position, follow=player)
    cam2 = Camera(world_position=player.position, follow=player, view_port=pygame.Rect(400, 0, 400, 300), zoom=0.75,
                  rotation=180)
    cam.offset.y = 100  # moves the ground line 100px down

    clock = pygame.Clock()
    fps_clock = pygame.Clock()
    fps_accu = 0
    last_blit_count = 0
    running = True
    while running:
        dt = clock.tick() / 1000.0
        # events
        for event in pygame.event.get():

            if event.type == pygame.QUIT:
                running = False
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    running = False
                elif event.key == pygame.K_SPACE:
                    if cam.zoom == 1.0:
                        cam.zoom = 2.0
                    else:
                        cam.zoom = 1.0
                elif event.key == pygame.K_n:
                    if light_spr.visible:
                        # set to day
                        light_spr.visible = False
                        light_spr2.visible = False
                        light_spr3.visible = False
                        sprites_drawer.ambient_color = None
                    else:
                        # set to night
                        light_spr.visible = True
                        light_spr2.visible = True
                        light_spr3.visible = True
                        sprites_drawer.ambient_color = (60, 60, 60)
                elif event.key == pygame.K_l:
                    sprites_drawer.limit_fps = not sprites_drawer.limit_fps
                    print("limit fps:", sprites_drawer.limit_fps)
                elif event.key == pygame.K_r:
                    if cam.rotation == 0.0:
                        cam.rotation = 30.0
                    else:
                        cam.rotation = 0.0

        # update
        pressed = pygame.key.get_pressed()
        r_pressed = pressed[pygame.K_RIGHT]
        l_pressed = pressed[pygame.K_LEFT]
        player.move((r_pressed - l_pressed), dt)

        fps_accu += dt
        if fps_accu > 1:  # every second
            print("fps:", fps_clock.get_fps(), "ups:", clock.get_fps(), "blit count:", last_blit_count,
                  "sprites count:", len(sprites_drawer._sprites),
                  # sprites3._get_image.cache_info(),
                  sprites_drawer.cache_info)
            fps_accu -= 1

        # draw
        if sprites_drawer.next_frame(dt, (cam, cam2)):
            render_result = sprites_drawer.render_sprites(screen, cam)
            render_result2 = []
            # render_result2 = sprites_drawer.render_sprites(screen, cam2, do_flip=True, fill_color=None)
            sprites_drawer.end_frame()
            last_blit_count = len(render_result) + len(render_result2)
            fps_clock.tick()

    pygame.quit()


if __name__ == '__main__':
    main()

# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'smoke3.py' is part of sprites2.0source
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
Smoke simulation using particles to test the sprite module performance.

Particles based on smoke effect in tutorials: https://github.com/tank-king/Tutorials
"""
from __future__ import print_function, division

import random
from typing import List

import pygame

import sprites3

SCREENSIZE = 800, 600


class Particle(sprites3.AbstractDrawData):

    def __init__(self, position, velocity):
        super().__init__(position)
        self.velocity = velocity
        self.alpha = 255 / 255.0
        self.alpha_rate = 3 / 255.0
        self.zoom = 0.1
        self.k = 0.01 * random.random() * random.choice([-1, 1])
        self.alive = True


IMAGE = None


class Emitter:
    def __init__(self, position, direction, renderer):
        self.position = position
        self.direction = direction
        self.renderer: sprites3.SpriteRenderer = renderer
        self.particles: List[Particle] = []
        self._frames = 0

    def update(self, dt):
        self.particles = [p for p in self.particles if p.alive]
        self._frames += 1
        if self._frames % 3 == 0:
            speed = 2 + random.randint(7, 10) / 10
            p = Particle(self.position.copy(), self.direction * speed)
            # p.layer = random.randint(0, 100)
            self.particles.append(p)
            spr = sprites3.EntitySprite(p, [IMAGE])
            spr.precision_size = 100
            spr.precision_alpha = 100
            self.renderer.add_sprite(spr)

        for p in self.particles:
            p.position += p.velocity
            p.velocity.x += p.k
            p.velocity.y *= 0.99
            p.zoom += 0.005
            p.alpha -= p.alpha_rate
            if p.alpha < 0:
                p.alpha = 0
                p.alive = False
            p.layer = 1.0 - p.alpha
            p.alpha_rate -= 0.1 / 255.0
            if p.alpha_rate < 1.5 / 255:
                p.alpha_rate = 1.5 / 255


class EmitterTimed:
    def __init__(self, position, direction, renderer):
        self.position = position
        self.direction = direction
        self.renderer: sprites3.SpriteRenderer = renderer
        self.particles: List[Particle] = []
        self._frames = 0
        self.accu = 0

    def update(self, dt):
        self.accu += dt
        _dt = 1 / 60.0  # 60 fps
        while self.accu > 0:
            self.accu -= _dt
            self.particles = [p for p in self.particles if p.alive]
            self._frames += 1
            if self._frames % 3 == 0:
                speed = 2 + random.randint(7, 10) / 10
                p = Particle(self.position.copy(), self.direction * speed * self.renderer.fps)
                self.particles.append(p)
                spr = sprites3.EntitySprite(p, [IMAGE])
                spr.precision_size = 100
                spr.precision_alpha = 100
                self.renderer.add_sprite(spr)

            dtt = _dt
            x = self.renderer.fps
            for p in self.particles:
                p.position += p.velocity * dtt
                p.velocity.x += p.k / dtt
                p.velocity.y *= 0.99 * dtt * x
                p.zoom += 0.005 * dtt * x
                p.alpha -= p.alpha_rate * dtt * x
                p.rotation += 45 * dtt
                if p.alpha < 0:
                    p.alpha = 0
                    p.alive = False
                p.layer = 1.0 - p.alpha
                p.alpha_rate -= 0.1 / 255.0 * dtt * x
                if p.alpha_rate < 1.0 / 255:
                    p.alpha_rate = 1.0 / 255


def main():
    pygame.init()
    pygame.display.set_caption("smoke demo, keys: -")
    # screen = pygame.display.set_mode(SCREENSIZE, pygame.RESIZABLE | pygame.SRCALPHA, 32)
    screen = pygame.display.set_mode(SCREENSIZE)
    global IMAGE
    IMAGE = pygame.image.load('../resources/smoke.png').convert_alpha()

    renderer = sprites3.SpriteRenderer()
    renderer.limit_fps = True

    emitter1 = Emitter(pygame.Vector2(SCREENSIZE[0] / 2, SCREENSIZE[1] / 2), pygame.Vector2(0, -1), renderer)  # center
    emitter4 = EmitterTimed(pygame.Vector2(SCREENSIZE[0] / 4, SCREENSIZE[1] / 2), pygame.Vector2(0, -1), renderer)
    emitter2 = Emitter(pygame.Vector2(SCREENSIZE[0] / 2, SCREENSIZE[1] / 2), pygame.Vector2(0, 1), renderer)  # center
    emitter3 = Emitter(pygame.Vector2(SCREENSIZE[0] / 4 * 3, SCREENSIZE[1] / 2), pygame.Vector2(-1, 0), renderer)  # mid
    emitter5 = EmitterTimed(pygame.Vector2(0, SCREENSIZE[1]), pygame.Vector2(1, -1).normalize(), renderer)  # bottom l
    emitter6 = Emitter(pygame.Vector2(SCREENSIZE[0], SCREENSIZE[1]), pygame.Vector2(-1, -1).normalize(), renderer)  # br

    emitter7 = EmitterTimed(pygame.Vector2(SCREENSIZE[0] / 2, SCREENSIZE[1] / 2), pygame.Vector2(1, -1).normalize(),
                            renderer)
    emitter8 = EmitterTimed(pygame.Vector2(SCREENSIZE[0] / 2, SCREENSIZE[1] / 2), pygame.Vector2(-1, 1).normalize(),
                            renderer)
    emitter9 = EmitterTimed(pygame.Vector2(SCREENSIZE[0] / 2, SCREENSIZE[1] / 2), pygame.Vector2(1, 1).normalize(),
                            renderer)

    emitters = [emitter1, emitter2, emitter3, emitter4, emitter5, emitter6,
                emitter7, emitter8, emitter9]

    surf = pygame.Surface((80, 60))
    surf.fill(pygame.Color("violet"))
    static = sprites3.StaticSprite(pygame.Vector2(SCREENSIZE[0] / 2, SCREENSIZE[1] / 4), [surf], layer=0.5)
    renderer.add_sprite(static)
    static = sprites3.StaticSprite(pygame.Vector2(SCREENSIZE[0] / 4, SCREENSIZE[1] / 4), [surf], layer=0.5)
    renderer.add_sprite(static)

    fps_accu = 0
    fps_clock = pygame.Clock()
    clock = pygame.Clock()
    running = True
    while running:
        dt = clock.tick(120) / 1000.0
        # events
        for event in pygame.event.get():

            if event.type == pygame.QUIT:
                running = False
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    running = False

        # update

        fps_accu += dt
        if fps_accu > 1:  # every second
            print("fps:", fps_clock.get_fps(), "ups:", clock.get_fps(), "particle count:",
                  sum(len(e.particles) for e in emitters),
                  "sprites count:", len(renderer._sprites),
                  # sprites3._get_image.cache_info(),
                  renderer.cache_info)
            fps_accu -= 1

        # draw
        if renderer.next_frame(dt):
            for e in emitters:
                e.update(renderer._dt)
            fps_clock.tick()
            screen.fill((0, 0, 0))
            renderer.render_sprites(screen)
            renderer.end_frame()

    pygame.quit()


if __name__ == '__main__':
    main()

# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'sprites2.py' is part of sprites2.0source repo.
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

from __future__ import division, print_function

import logging
from functools import lru_cache
from typing import List

import pygame

logger = logging.getLogger(__name__)


class Sprite:
    has_draw_method = False  # if True then a draw method `def draw(self, screen, cam)` on the sprite is called
    is_light = False  # decide if it should be handles as a light
    is_lit = True  # if True, lights illuminate this sprite, false sprite as is (fully lit) # todo invert bool value?
    is_hud = False  # if True, use viewport coordinates, otherwise use cam transformation
    special_flags = 0
    area = None
    anim_state = True  # if True always draw this sprite, otherwise only if corresponding entity is in same state

    def __init__(self, world_position, images, fps=0.0, offset=pygame.Vector2(0, 0),
                 alpha_layer=pygame.Vector2(1.0, 0), flip_xy=pygame.Vector2(0, 0), roto_zoom=pygame.Vector2(0, 1.0),
                 visible_anim_speed=pygame.Vector2(1, 1.0), anim_state=None, scale=pygame.Vector2(1.0, 1.0),
                 parallax=pygame.Vector2(1.0, 1.0), area=None, special_flags=0, is_hud=False,
                 entity=None):
        assert len(images) >= 1, "at least one image should be provided"

        # sharable
        self.position: pygame.Vector2 = world_position
        self.alpha_layer = alpha_layer
        self.flip_xy = flip_xy
        self.scale = scale
        self.roto_zoom = roto_zoom
        self.visible_anim_speed = visible_anim_speed
        self.offset: pygame.Vector2 = offset
        self.parallax = parallax

        # spr specific
        self.entity = entity  # todo if this is set, what is used from there? All shareables? is this really needed?
        self.anim_state = anim_state  # this only makes sense with entity set

        self.source_images = images
        self.fps = fps
        self.frame_time = 1.0 / (fps + 1e-6)
        self.current_idx = 0

        self.area = area
        self.special_flags = special_flags
        self.is_hud = is_hud


class LightSprite(Sprite):
    is_light = True

    def __init__(self, world_position, images, effect_images, alpha_layer=pygame.Vector2(1.0, 0),
                 offset=pygame.Vector2(0, 0), visible_anim_speed=pygame.Vector2(1, 1.0)):
        assert len(effect_images) == len(images), "same number of images and effect_images should be provided"
        super().__init__(world_position, images, alpha_layer=alpha_layer, special_flags=pygame.BLEND_RGBA_ADD,
                         offset=offset, visible_anim_speed=visible_anim_speed)
        self.effect_images = effect_images


class TextSprite(Sprite):

    def __init__(self, text, font, entity, offset, alpha_layer=pygame.Vector2(1.0, 0)):
        self._font: pygame.font.Font = font
        self._text = None
        self.text = text
        super().__init__(pygame.Vector2(0, 0), [pygame.Surface((0, 0))], offset=offset, entity=entity,
                         alpha_layer=alpha_layer)

    @property
    def text(self):
        return self._text

    @text.setter
    def text(self, value):
        self.source_images = [self._font.render(self.text, True, pygame.Color("black"))]
        self._text = value

    # def draw(self, screen, cam):
    #     label = self.font.render(self.text, True, pygame.Color("black"))
    #     pos = self.entity.position + self.offset
    #     r = label.get_rect(center=pos)
    #     return screen.blit(label, r)


class Camera:

    def __init__(self, world_position=None, view_port=None, zoom=1.0, follow=None, jump_to=False, rotation=0.0):
        self.view_port = view_port or pygame.display.get_surface().get_rect()  # rect on screen in screen coordinates, area in which this cam draws
        self.center = pygame.Vector2(*self.view_port.topleft) + pygame.Vector2(
            *self.view_port.size) / 2  # center in self.screen coordinates
        self.position: pygame.Vector2 = self.center.copy() if world_position is None else world_position.copy()  # position in the world
        self.target_position = self.position.copy()  # position in the world
        self.smooth_radius = 150
        self.speed = 500
        self.offset = pygame.Vector2(0.0, 0.0)  # use this for a shake effect or similar
        self.zoom = zoom
        self.zoom_offset = 0.0
        self.anchor = pygame.Vector2(0.0, 0.0)  # used to set a different parallax point
        self.follow = follow
        self.jump_to = jump_to
        self.rotation = rotation
        self.rotation_offset = 0  # use this for some offset, e.g. shake effect

    def follow(self, entity):
        assert hasattr(entity, 'position'), "target should have a position attribute"
        self.follow = entity

    # Tell the camera to pan to the given position.
    def lookat(self, world_pos, immediate=False):
        self.follow(None)  # do not follow anymore
        self.target_position.update(world_pos)
        if immediate:
            self.position.update(self.target_position)

    def update(self, dt):
        if self.follow:
            self.target_position.update(self.follow.position)
        # arrive smoothly to target
        distance = self.position.distance_to(self.target_position)
        f = distance / self.smooth_radius
        if f > 1:
            f = 1.0
            if self.jump_to:
                self.position.update(self.target_position)
                return
        else:
            f = f
        self.position.move_towards_ip(self.target_position, self.speed * dt * f)

    def to_screen(self, world_position, parallax_factors=pygame.Vector2(1.0, 1.0)):
        parallax_offset = (parallax_factors - pygame.Vector2(1.0, 1.0)).elementwise() * (
                world_position - (self.position + self.anchor))
        position = world_position + parallax_offset + self.offset
        return self.center + (self.zoom + self.zoom_offset) * (position - self.position).rotate(
            self.rotation + self.rotation_offset)

    def to_world(self, screen_position):
        if self.zoom == 0:
            self.zoom = 0.0001  # avoid division by 0
        return (screen_position - self.center).rotate(-self.rotation - self.rotation_offset) / (
                self.zoom + self.zoom_offset) + self.position - self.offset


# todo add sprite groups to manage many sprites at once: add/remove/visible/... at once?
# todo mouse picking of sprites -> sprites need to be stored in SpritesDrawer
# todo lights
# todo is that a good limit? !If the cache is too big then it takes longer to check for hit or miss!
@lru_cache(maxsize=2000)
def _get_image(_alpha100x, _flip_x, _flip_y, _rotation, _scale_x100x, _scale_y100x, _zoom100x, img):
    img = img.copy()
    if _alpha100x != 100.0:
        img.fill((255, 255, 255, _alpha100x / 100 * 255), None, pygame.BLEND_RGBA_MULT)
    if _flip_x or _flip_y:
        img = pygame.transform.flip(img, _flip_x, _flip_y)
    if _scale_x100x != 100.0 or _scale_y100x != 100.0:
        w, h = img.get_size()
        img = pygame.transform.scale(img, (_scale_x100x / 100 * w, _scale_y100x / 100 * h))
    if _rotation or _zoom100x != 100.0:
        img = pygame.transform.rotozoom(img, -_rotation, _zoom100x / 100)
    return img


class SpritesDrawer:

    def __init__(self, fps=60, ambient_color=None):
        self.default_cam = Camera()
        self.fps = fps
        self.frame_time = 1.0 / self.fps
        self.accu = 100000  # draw on first call
        self.ambient_color = ambient_color

    def render_sprites1(self, screen: pygame.Surface, sprites: List[EntitySprite], dt: float, cam=None):
        self.accu += dt
        if self.accu < self.frame_time:
            return None
        self.accu -= self.frame_time

        rects = []
        # todo: only sort if needed -> list has to be kept here
        _sorted_sprites = sorted(sprites, key=lambda spr_: (spr_.layer, spr_.entity.position.y))
        cam = cam or self.default_cam
        orig_clip = screen.get_clip()
        screen.set_clip(cam.view_port)
        for spr in _sorted_sprites:  # todo: only draw potentially visible sprites -> depends on cam!!
            entity = spr.entity
            visible = entity.visible and spr.anim_state == entity.anim_state

            if not visible:
                continue

            # draw
            if spr.has_draw_method:
                r = spr.draw(screen, cam)
                assert isinstance(r, pygame.Rect), "return of draw method should be dirty rect"
                rects.append(r)
                continue

            if len(spr.source_images) > 1:
                spr.accu += dt
                if spr.accu >= spr.frame_time:  # todo use 'while' instead of 'if' to skip frames?
                    # todo: if anim_speed is set near 0 this will never recover.... (how to prevent that?)!
                    spr.accu -= (spr.frame_time / entity.anim_speed)
                    spr.current_idx += 1
                    if spr.current_idx >= len(spr.source_images):
                        # todo add optional callback after animation end -> auto remove option?
                        spr.current_idx -= len(spr.source_images)
                    spr.image = spr.source_images[spr.current_idx]

            img = spr.image  # todo update spr.image to corresponding frame if animated: spr.fps * spr.entity.anim_speed
            _flip_x = entity.flip_x
            _flip_y = entity.flip_y
            _rotation = entity.rotation  # todo: round to resolution
            _zoom = entity.zoom  # todo: round to resolution
            _scale = entity.scale  # todo: round to resolution
            _alpha = entity.alpha

            if entity.is_hud:
                _screen_position = entity.position
            else:
                _zoom *= (cam.zoom + cam.zoom_offset)  # todo: round to resolution
                _rotation += cam.rotation + cam.rotation_offset  # todo: round to resolution
                _screen_position = cam.to_screen(entity.position,
                                                 entity.parallax)  # todo convert to screen position

            # todo cache following transformations
            #  key = (img, _flip_x, _flip_y, _rotation, _zoom, _scale.x, _scale_y, _alpha)
            # img, _screen_offset = transform(*key)
            _screen_offset = _scale.elementwise() * spr.offset * _zoom
            _screen_offset = _screen_offset.rotate(_rotation)
            if _alpha != 1.0:
                img = img.copy()
                img.fill((255, 255, 255, _alpha * 255), None, pygame.BLEND_RGBA_MULT)
            if _flip_x or _flip_y:
                img = pygame.transform.flip(img, _flip_x, _flip_y)
            if _scale.x != 1.0 or _scale.y != 1.0:
                img = pygame.transform.scale(img, (_scale.elementwise() * img.get_size()))
            if _rotation or _zoom != 1.0:
                img = pygame.transform.rotozoom(img, -_rotation, _zoom)

            center = _screen_position - _screen_offset
            pos = img.get_rect(center=center)
            spr.blit_rect = screen.blit(img, pos, spr.area, spr.special_flags)  # todo add special blend flags
            rects.append(spr.blit_rect)

        screen.set_clip(orig_clip)
        return rects

    def render_sprites(self, screen: pygame.Surface, sprites: List[EntitySprite], dt: float, cam=None):
        self.accu += dt
        if self.accu < self.frame_time:
            return None
        self.accu -= self.frame_time

        rects = []
        # todo: only sort if needed -> list has to be kept here
        _sorted_sprites = sorted(sprites, key=lambda spr_: (spr_.layer, spr_.entity.position.y, spr_.is_light))
        # todo remove not visible sprites here already, here?
        # todo remove sprites outside of view area here already, here?
        #      only draw potentially visible sprites -> depends on cam!!

        cam = cam or self.default_cam
        orig_clip = screen.get_clip()
        screen.set_clip(cam.view_port)

        if self.ambient_color:
            self._render_with_light(_sorted_sprites, cam, dt, rects, screen)
        else:
            # todo should is_light sprites be filtered out here?
            self._render_sprites(_sorted_sprites, None, cam, dt, rects, screen)

        screen.set_clip(orig_clip)
        return rects

    def _render_with_light1(self, _sorted_sprites, cam, dt, rects, screen):
        sprite_groups = []
        sprite_group = []
        light_groups = []
        light_group = []
        last_is_light = None
        for spr in _sorted_sprites:
            if spr.is_light:
                light_group.append(spr)
                sprite_group.append(spr)
                if last_is_light is not None and not last_is_light:
                    sprite_groups.append(sprite_group)
                    sprite_group = []
                last_is_light = True
            else:
                sprite_group.append(spr)
                if last_is_light is not None and last_is_light:
                    light_groups.append(light_group)
                    light_group = []
                last_is_light = False
        if sprite_group:
            sprite_groups.append(sprite_group)
        if light_group:
            light_groups.append(light_group)
        light_groups.append([])  # add top layer without any lights
        ambient_surf = screen.copy()
        ambient_surf.set_clip(cam.view_port)

        for sprite_group in sprite_groups:
            ambient_surf.fill(self.ambient_color)
            for light_group in light_groups:
                self._render_sprites(light_group, None, cam, dt, rects, ambient_surf)
            light_groups.pop(0)

            self._render_sprites(sprite_group, ambient_surf, cam, dt, rects, screen)

    def _render_with_light(self, _sorted_sprites, cam, dt, rects, screen):
        sprite_groups = []
        sprite_group = []
        light_groups = []
        light_group = []
        last_is_light = None
        for spr in _sorted_sprites:
            if spr.is_light:
                light_group.append(spr)
                # sprite_group.append(spr)
                if last_is_light is not None and not last_is_light:
                    sprite_groups.append(sprite_group)
                    sprite_group = []
                last_is_light = True
            else:
                sprite_group.append(spr)
                if last_is_light is not None and last_is_light:
                    light_groups.append(light_group)
                    light_group = []
                last_is_light = False
        if sprite_group:
            sprite_groups.append(sprite_group)
        if light_group:
            light_groups.append(light_group)
        light_groups.append([])  # add top layer without any lights
        ambient_surf = pygame.Surface(screen.get_size(), pygame.SRCALPHA)
        ambient_surf.set_clip(cam.view_port)

        screen_copy = pygame.Surface(screen.get_size(), pygame.SRCALPHA)  # need the alpha!!
        screen_copy.set_clip(cam.view_port)
        for sprite_group in sprite_groups:
            ambient_surf.fill(self.ambient_color)
            for light_group in light_groups:
                self._render_sprites(light_group, None, cam, dt, rects, ambient_surf)

            self._render_sprites(sprite_group, None, cam, dt, rects, screen_copy)

            screen_copy.blit(ambient_surf, (0, 0), None, pygame.BLEND_RGBA_MULT)

            screen.blit(screen_copy, (0, 0), None, 0)
            self._render_sprites(light_groups[0], None, cam, dt, rects, screen)
            light_groups.pop(0)
            screen_copy.fill((0, 0, 0, 0), None, pygame.BLEND_RGBA_MULT)

    def _render_sprites(self, _sorted_sprites, ambient_surf, cam, dt, rects, screen):
        for spr in _sorted_sprites:
            entity = spr.entity
            visible = entity.visible and (spr.anim_state == entity.anim_state or spr.anim_state is True)

            if not visible:
                continue

            # draw
            if spr.has_draw_method:
                r = spr.draw(screen, cam)
                assert isinstance(r, pygame.Rect), "return of draw method should be dirty rect"
                rects.append(r)
                continue

            if len(spr.source_images) > 1:
                spr.accu += dt
                while spr.accu >= spr.frame_time:  # todo use 'while' instead of 'if' to skip frames?
                    # todo: if anim_speed is set near 0 this will never recover.... (how to prevent that?)!
                    spr.accu -= (spr.frame_time / entity.anim_speed)
                    spr.current_idx += 1
                    if spr.current_idx >= len(spr.source_images):
                        # todo add optional callback after animation end -> auto remove option?
                        spr.current_idx -= len(spr.source_images)
                    spr.image = spr.source_images[spr.current_idx]

            _flip_x = entity.flip_x
            _flip_y = entity.flip_y
            _rotation = entity.rotation  # todo: round to resolution
            _zoom = entity.zoom  # todo: round to resolution
            _scale = entity.scale  # todo: round to resolution
            _alpha = entity.alpha

            if entity.is_hud:
                _screen_position = entity.position
            else:
                _zoom *= (cam.zoom + cam.zoom_offset)  # todo: round to resolution
                _rotation += cam.rotation + cam.rotation_offset  # todo: round to resolution
                _screen_position = cam.to_screen(entity.position,
                                                 entity.parallax)  # todo convert to screen position

            _screen_offset = _scale.elementwise() * spr.offset * _zoom
            _screen_offset = _screen_offset.rotate(_rotation)
            center = _screen_position - _screen_offset

            # todo cache following transformations
            _scale_x, _scale_y = _scale.xy
            if spr.is_light:
                img = spr.effect
            else:
                img = spr.image
            # key = (img, _flip_x, _flip_y, _rotation, _zoom, _scale.x, _scale.y, _alpha)
            # key2 = (_alpha, _flip_x, _flip_y, _rotation, _scale, _zoom, img)
            # img, _screen_offset = transform(*key)
            # if _alpha != 1.0:
            #     img = img.copy()
            #     img.fill((255, 255, 255, _alpha * 255), None, pygame.BLEND_RGBA_MULT)
            # if _flip_x or _flip_y:
            #     img = pygame.transform.flip(img, _flip_x, _flip_y)
            # if _scale_x != 1.0 or _scale_y != 1.0:
            #     w, h = img.get_size()
            #     img = pygame.transform.scale(img, (_scale_x * w, _scale_y * h))
            # if _rotation or _zoom != 1.0:
            #     img = pygame.transform.rotozoom(img, -_rotation, _zoom)

            # cut to certain precision otherwise each float value would generate a miss in the cache even if the
            # difference is small and visually not noticeable. Event so there will be enough entries to cache!
            img = _get_image(int(_alpha * 100),  # 0.01 precision -> 255/100 = 2.55 steps
                             _flip_x,  # bool
                             _flip_y,  # bool
                             int(_rotation),  # 1 degree precision
                             int(_scale_x * 100),  # 0.01 precision  is that enough?
                             int(_scale_y * 100),  # 0.01 precision is that enough?
                             int(_zoom * 100),  # 0.01 precision is that enough?
                             img)

            pos = img.get_rect(center=center)
            if ambient_surf and not spr.is_light and spr.is_lit:
                img = img.copy()
                # img.fill(self.ambient_color, None, pygame.BLEND_RGBA_MULT)
                img.blit(ambient_surf, (0, 0), pos, pygame.BLEND_RGB_MULT)
            spr.blit_rect = screen.blit(img, pos, spr.area, spr.special_flags)
            # if tuple(pos) != tuple(spr.blit_rect):
            #     raise Exception()
            rects.append(spr.blit_rect)

# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file '2.5dview.py' is part of sprites2.0source
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
Demo showing a 2.5D view implementation.


"""
from __future__ import print_function, division

import logging
import math
import random
from enum import Enum
from functools import cache

import pygame

from sprites3 import AbstractDrawData, EntitySprite, StaticSprite, SpriteRenderer

logging.basicConfig()

logger = logging.getLogger(__name__)

SCREENSIZE = 800, 600


class Tree(AbstractDrawData):

    def __init__(self, position, radius, rotation_degrees, layer):
        super().__init__(position, rotation_degrees)
        self.radius = radius


class AnimState(Enum):
    W = 0
    NW = 1
    N = 2
    NE = 3
    E = 4
    SE = 5
    S = 6
    SW = 7
    Idle_W = 0 + 8
    Idle_NW = 1 + 8
    Idle_N = 2 + 8
    Idle_NE = 3 + 8
    Idle_E = 4 + 8
    Idle_SE = 5 + 8
    Idle_S = 6 + 8
    Idle_SW = 7 + 8


class Light(AbstractDrawData):

    def __init__(self, position):
        super().__init__(world_position=position)


class Ant(AbstractDrawData):

    def __init__(self, position, rotation_degrees, layer):
        super().__init__(world_position=position, rotation_degrees=rotation_degrees)
        self.anim_state = AnimState.W
        self.direction = pygame.Vector2(-1, 0)
        self.speed = 64
        self.anim_speed = 2.0
        self.target_position = position.copy()
        self.radius = 15
        self.light = Light(position)
        self.old_position = pygame.Vector2(0, 0)

    def update(self, dt):
        anim_state = self.get_anim_state()
        self.light.rotation = anim_state * 45 - 90
        if self.position.distance_to(self.target_position) < 5.0:
            self.anim_state = AnimState(anim_state + 8)
            return
        self.direction = (self.target_position - self.position)
        self.anim_state = AnimState(anim_state)
        if self.direction.length_squared():
            self.direction.normalize_ip()
        self.old_position.update(self.position)
        self.position += self.speed * self.anim_speed * self.direction * dt
        return self.position.y - self.old_position.y != 0

    def get_anim_state(self):
        angle_rad = math.atan2(self.direction.y, self.direction.x) + math.pi
        angle = (math.degrees(angle_rad) + 45 / 2) % 360
        return int(angle // 45)


@cache
def get_images_from_row(sprite_sheet, x, y, count):
    r = pygame.Rect(0, 0, 128, 128)
    images = []
    for i in range(count):
        surf = pygame.Surface(r.size, pygame.SRCALPHA)
        surf.blit(sprite_sheet, (0, 0), r.move((i + x) * r.w, y * r.h))
        images.append(surf)

    return images


def create_ant(sheet):
    ant = Ant(pygame.Vector2(400, 570), 0, 0)
    # rows: W, NW, N, NE, E, SE, S, SW  cols: 0-3 stance, 4-11 walk, ...
    y_offset = 20
    walk_w = EntitySprite(entity=ant, images=get_images_from_row(sheet, 4, 0, 8), fps=10,
                          offset=pygame.Vector2(0, y_offset), anim_state=AnimState.W)
    walk_nw = EntitySprite(entity=ant, images=get_images_from_row(sheet, 4, 1, 8), fps=10,
                           offset=pygame.Vector2(0, y_offset), anim_state=AnimState.NW)
    walk_n = EntitySprite(entity=ant, images=get_images_from_row(sheet, 4, 2, 8), fps=10,
                          offset=pygame.Vector2(0, y_offset), anim_state=AnimState.N)
    walk_ne = EntitySprite(entity=ant, images=get_images_from_row(sheet, 4, 3, 8), fps=10,
                           offset=pygame.Vector2(0, y_offset), anim_state=AnimState.NE)
    walk_e = EntitySprite(entity=ant, images=get_images_from_row(sheet, 4, 4, 8), fps=10,
                          offset=pygame.Vector2(0, y_offset), anim_state=AnimState.E)
    walk_se = EntitySprite(entity=ant, images=get_images_from_row(sheet, 4, 5, 8), fps=10,
                           offset=pygame.Vector2(0, y_offset), anim_state=AnimState.SE)
    walk_s = EntitySprite(entity=ant, images=get_images_from_row(sheet, 4, 6, 8), fps=10,
                          offset=pygame.Vector2(0, y_offset), anim_state=AnimState.S)
    walk_sw = EntitySprite(entity=ant, images=get_images_from_row(sheet, 4, 7, 8), fps=10,
                           offset=pygame.Vector2(0, y_offset), anim_state=AnimState.SW)
    idle_w = EntitySprite(entity=ant, images=get_images_from_row(sheet, 0, 0, 4), fps=10,
                          offset=pygame.Vector2(0, y_offset), anim_state=AnimState.Idle_W)
    idle_nw = EntitySprite(entity=ant, images=get_images_from_row(sheet, 0, 1, 4), fps=10,
                           offset=pygame.Vector2(0, y_offset), anim_state=AnimState.Idle_NW)
    idle_n = EntitySprite(entity=ant, images=get_images_from_row(sheet, 0, 2, 4), fps=10,
                          offset=pygame.Vector2(0, y_offset), anim_state=AnimState.Idle_N)
    idle_ne = EntitySprite(entity=ant, images=get_images_from_row(sheet, 0, 3, 4), fps=10,
                           offset=pygame.Vector2(0, y_offset), anim_state=AnimState.Idle_NE)
    idle_e = EntitySprite(entity=ant, images=get_images_from_row(sheet, 0, 4, 4), fps=10,
                          offset=pygame.Vector2(0, y_offset), anim_state=AnimState.Idle_E)
    idle_se = EntitySprite(entity=ant, images=get_images_from_row(sheet, 0, 5, 4), fps=10,
                           offset=pygame.Vector2(0, y_offset), anim_state=AnimState.Idle_SE)
    idle_s = EntitySprite(entity=ant, images=get_images_from_row(sheet, 0, 6, 4), fps=10,
                          offset=pygame.Vector2(0, y_offset), anim_state=AnimState.Idle_S)
    idle_sw = EntitySprite(entity=ant, images=get_images_from_row(sheet, 0, 7, 4), fps=10,
                           offset=pygame.Vector2(0, y_offset), anim_state=AnimState.Idle_SW)

    sprites = [
        walk_w, walk_nw, walk_n, walk_ne, walk_e, walk_se, walk_s, walk_sw,
        idle_w, idle_nw, idle_n, idle_ne, idle_e, idle_se, idle_s, idle_sw,
    ]
    return ant, sprites


def main():
    pygame.init()

    pygame.display.set_caption("2.5D demo special sort order, click with mouse, keys: space, r, n")
    screen = pygame.display.set_mode(SCREENSIZE)

    sheet = pygame.image.load("../resources/antlion/antlion_0.png")
    ant, ant_sprites = create_ant(sheet)

    light_mask = pygame.Surface((100, 100), pygame.SRCALPHA)
    light_mask.fill((0, 0, 0, 0))
    radius = 50
    pygame.draw.circle(light_mask, (150, 150, 150, 255), (radius, radius), radius)
    effect = light_mask.copy()
    effect.fill((100, 100, 100), None, pygame.BLEND_RGB_SUB)

    light_spr = EntitySprite(entity=ant.light, images=[light_mask])
    light_spr.effect_images = [effect]
    light_spr.is_light = True
    light_spr.special_flags = pygame.BLEND_RGBA_ADD
    light_spr.layer = 0  # important: has to on the same layer as the ant and the trees so it gets sorted by y coord!
    light_spr.offset = pygame.Vector2(0, radius)
    light_spr.entity().visible = False  # start with day

    sprites = list(ant_sprites)
    sprites.append(light_spr)

    tree_images = [
        pygame.image.load("../resources/trees/Sprite_01.png").convert_alpha(),
    ]

    trees = []
    tree_sprites = []
    create_trees(tree_images, tree_sprites, trees)

    sprites.extend(tree_sprites)

    sand_tile = pygame.image.load("../resources/sand_tile.png").convert()
    for x in range(0, 800, 32):
        for y in range(0, 600, 32):
            spr = StaticSprite(world_position=pygame.Vector2(x, y), images=[sand_tile], offset=pygame.Vector2(-16, -16),
                               layer=-1)
            sprites.append(spr)

    show_collision_radius = False
    sprites_drawer = SpriteRenderer()
    # sprites_drawer.always_sort = True  # because of the y coordinate of the moving ant
    sprites_drawer.add_sprites(sprites)

    clock = pygame.Clock()
    running = True
    while running:
        dt = clock.tick(60) / 1000.0  # convert to seconds
        # events
        for event in pygame.event.get():

            if event.type == pygame.QUIT:
                running = False
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    running = False
                elif event.key == pygame.K_SPACE:
                    show_collision_radius = not show_collision_radius
                elif event.key == pygame.K_r:
                    for t_ in tree_sprites:
                        sprites_drawer.remove_sprite(t_)
                    trees.clear()
                    tree_sprites.clear()
                    create_trees(tree_images, tree_sprites, trees)
                    sprites_drawer.add_sprites(tree_sprites)
                elif event.key == pygame.K_n:
                    if light_spr.entity().visible:
                        # set to day
                        light_spr.entity().visible = False
                        sprites_drawer.ambient_color = None
                    else:
                        # set to night
                        light_spr.entity().visible = True
                        sprites_drawer.ambient_color = (60, 60, 60, 255)
            elif event.type == pygame.MOUSEBUTTONDOWN:
                ant.target_position.update(*event.pos)

        # update
        moved_y = ant.update(dt)
        if moved_y:
            sprites_drawer._need_sort = True

        for t in trees:
            if t.position.distance_to(ant.position) < (t.radius + ant.radius):
                d = ant.position - t.position
                depth = (t.radius + ant.radius) - d.length()
                ant.position += d.normalize() * depth

        # draw
        if sprites_drawer.next_frame(dt):
            screen.fill((255, 0, 255))
            sprites_drawer.render_sprites(screen)

            if show_collision_radius:
                pygame.draw.circle(screen, pygame.Color("white"), ant.position, ant.radius, 1)
                for t in trees:
                    pygame.draw.circle(screen, pygame.Color("white"), t.position, t.radius, 1)

            sprites_drawer.end_frame()

    pygame.quit()


def create_trees(tree_images, tree_sprites, trees):
    while len(trees) < 10:
        position = pygame.Vector2(random.randint(50, 750), random.randint(200, 600))
        tree = Tree(position, radius=10, rotation_degrees=0, layer=0)
        tree.zoom = 0.6
        # important: has to on the same layer as the ant and the trees so it gets sorted by y coord!
        tree_sprite = EntitySprite(entity=tree, images=[random.choice(tree_images)], fps=0,
                                   offset=pygame.Vector2(94, 238), anim_state=None, layer=0)
        # tree_sprite = EntitySprite(tree, [random.choice(tree_images)], 0,
        #                            pygame.Vector2(0, 75), None)
        # spread them wide enough
        if not any((t_ for t_ in trees if t_.position.distance_to(tree.position) < 150)):
            trees.append(tree)
            tree_sprites.append(tree_sprite)


if __name__ == '__main__':
    main()

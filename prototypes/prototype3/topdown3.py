# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'topdown.py' is part of sprites2.0source
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
sidescroller.py to show how a different draw system could work.

"""
from __future__ import print_function, division

import logging
import math
import random
from enum import Enum
from functools import cache

import pygame

from sprites3 import EntitySprite, AbstractDrawData, SpriteRenderer, StaticSprite

logging.basicConfig()

logger = logging.getLogger(__name__)

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(_d) for _d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2025"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

__all__ = []  # list of public visible parts of this module

SCREENSIZE = 800, 600


class AnimState(Enum):
    W = 0
    NW = 1
    N = 2
    NE = 3
    E = 4
    SE = 5
    S = 6
    SW = 7
    Idle_W = 0 + 8
    Idle_NW = 1 + 8
    Idle_N = 2 + 8
    Idle_NE = 3 + 8
    Idle_E = 4 + 8
    Idle_SE = 5 + 8
    Idle_S = 6 + 8
    Idle_SW = 7 + 8


class Ant(AbstractDrawData):

    def __init__(self, position, rotation_degrees, layer):
        super().__init__(position, rotation_degrees)
        self.anim_state = AnimState.W
        self.direction = pygame.Vector2(-1, 0).rotate(random.randint(0, 360))
        self.speed = 64
        self.anim_speed = 1.0
        self.accu = 0
        self.duration = 0

    def update(self, dt):
        self.position += self.speed * self.anim_speed * self.direction * dt
        self.accu += dt
        if self.position.x < 0:
            self.position.x = 800
        if self.position.x > 800:
            self.position.x = 0
        if self.position.y < 0:
            self.position.y = 600
        if self.position.y > 600:
            self.position.y = 0

        if self.accu >= self.duration:
            print("update!")
            self.accu = 0
            z = 0.25
            if self.anim_state in (
                    AnimState.Idle_W, AnimState.Idle_NW, AnimState.Idle_N, AnimState.Idle_NE, AnimState.Idle_E,
                    AnimState.Idle_SE, AnimState.Idle_S, AnimState.Idle_SW
            ):
                z = 0.95

            if random.random() >= z:
                # pause
                self.duration = random.randint(0, 2)
                self.speed = 0
                self.anim_state = AnimState(self.get_anim_state() + 8)  # +8 are the idle states
                print("pause: ", self.duration, "anim:", self.anim_state)
            else:
                self.duration = random.randint(0, 5)
                self.direction.rotate_ip(random.randint(-45, 45))
                self.anim_state = AnimState(self.get_anim_state())
                # self.speed = random.randint(1, 20)
                self.speed = 64  # keep this on the default speed and adjust self.anim_speed instead
                self.anim_speed = random.randint(1, 4)
                print("move: ", self.duration, "anim:", self.anim_state, "speed:", self.speed * self.anim_speed, "dir:",
                      self.direction)

    def get_anim_state(self):
        angle_rad = math.atan2(self.direction.y, self.direction.x) + math.pi
        angle = (math.degrees(angle_rad) + 45 / 2) % 360
        return int(angle // 45)


@cache
def get_images_from_row(sprite_sheet, x, y, count):
    r = pygame.Rect(0, 0, 128, 128)
    images = []
    for i in range(count):
        surf = pygame.Surface(r.size, pygame.SRCALPHA)
        surf.blit(sprite_sheet, (0, 0), r.move((i + x) * r.w, y * r.h))
        images.append(surf)

    return images


def main():
    pygame.init()
    pygame.display.set_caption("topdown demo, variable animation speed")
    screen = pygame.display.set_mode(SCREENSIZE)

    ants = []
    sprites = []

    sheet = pygame.image.load("../resources/antlion/antlion_0.png")
    for i in range(10):
        ant, ant_anims = create_ant(sheet)
        ants.append(ant)
        sprites.extend(ant_anims)

    # background
    sand_tile = pygame.image.load("../resources/sand_tile.png").convert()
    for x in range(0, 800, 32):
        for y in range(0, 600, 32):
            spr = StaticSprite(world_position=pygame.Vector2(x, y), images=[sand_tile], offset=pygame.Vector2(-16, -16),
                               layer=-1)
            sprites.append(spr)

    sprites_drawer = SpriteRenderer()
    sprites_drawer.add_sprites(sprites)

    clock = pygame.Clock()
    running = True
    while running:
        dt = clock.tick(60) / 1000.0  # convert to seconds
        # events
        for event in pygame.event.get():

            if event.type == pygame.QUIT:
                running = False
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    running = False

        # update
        for a in ants:
            a.update(dt)

        # draw
        # sprites
        if sprites_drawer.next_frame(dt):
            sprites_drawer.render_sprites(screen)
            # # grid
            # for i in range(0, 800, 20):
            #     pygame.draw.line(screen, pygame.Color("darkgreen"), (i, 0), (i, 600), 1)
            # for i in range(0, 600, 20):
            #     pygame.draw.line(screen, pygame.Color("darkgreen"), (0, i), (800, i), 1)
            sprites_drawer.end_frame()

    pygame.quit()


def create_ant(sheet):
    ant = Ant(pygame.Vector2(800, 300), 0, 0)
    # rows: W, NW, N, NE, E, SE, S, SW  cols: 0-3 stance, 4-11 walk, ...
    walk_w = EntitySprite(entity=ant, images=get_images_from_row(sheet, 4, 0, 8), fps=10, anim_state=AnimState.W)
    walk_nw = EntitySprite(entity=ant, images=get_images_from_row(sheet, 4, 1, 8), fps=10, anim_state=AnimState.NW)
    walk_n = EntitySprite(entity=ant, images=get_images_from_row(sheet, 4, 2, 8), fps=10, anim_state=AnimState.N)
    walk_ne = EntitySprite(entity=ant, images=get_images_from_row(sheet, 4, 3, 8), fps=10, anim_state=AnimState.NE)
    walk_e = EntitySprite(entity=ant, images=get_images_from_row(sheet, 4, 4, 8), fps=10, anim_state=AnimState.E)
    walk_se = EntitySprite(entity=ant, images=get_images_from_row(sheet, 4, 5, 8), fps=10, anim_state=AnimState.SE)
    walk_s = EntitySprite(entity=ant, images=get_images_from_row(sheet, 4, 6, 8), fps=10, anim_state=AnimState.S)
    walk_sw = EntitySprite(entity=ant, images=get_images_from_row(sheet, 4, 7, 8), fps=10, anim_state=AnimState.SW)
    idle_w = EntitySprite(entity=ant, images=get_images_from_row(sheet, 0, 0, 4), fps=10, anim_state=AnimState.Idle_W)
    idle_nw = EntitySprite(entity=ant, images=get_images_from_row(sheet, 0, 1, 4), fps=10, anim_state=AnimState.Idle_NW)
    idle_n = EntitySprite(entity=ant, images=get_images_from_row(sheet, 0, 2, 4), fps=10, anim_state=AnimState.Idle_N)
    idle_ne = EntitySprite(entity=ant, images=get_images_from_row(sheet, 0, 3, 4), fps=10, anim_state=AnimState.Idle_NE)
    idle_e = EntitySprite(entity=ant, images=get_images_from_row(sheet, 0, 4, 4), fps=10, anim_state=AnimState.Idle_E)
    idle_se = EntitySprite(entity=ant, images=get_images_from_row(sheet, 0, 5, 4), fps=10, anim_state=AnimState.Idle_SE)
    idle_s = EntitySprite(entity=ant, images=get_images_from_row(sheet, 0, 6, 4), fps=10, anim_state=AnimState.Idle_S)
    idle_sw = EntitySprite(entity=ant, images=get_images_from_row(sheet, 0, 7, 4), fps=10, anim_state=AnimState.Idle_SW)
    sprites = [
        walk_w, walk_nw, walk_n, walk_ne, walk_e, walk_se, walk_s, walk_sw,
        idle_w, idle_nw, idle_n, idle_ne, idle_e, idle_se, idle_s, idle_sw,
    ]
    return ant, sprites


if __name__ == '__main__':
    main()

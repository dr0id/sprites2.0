# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'sidescroller.py' is part of sprites2.0source
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
sidescroller.py to show how a different draw system could work.


"""
from __future__ import print_function, division

import logging
import math
from enum import Enum

import pygame

from prototypes.prototype1.sprites2 import AbstractEntityDrawData, EntitySprite, TextSprite, StaticSprite, SpritesDrawer

logging.basicConfig()
logger = logging.getLogger(__name__)

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(_d) for _d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2025"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

SCREENSIZE = 800, 600


class AnimState(Enum):
    Idle = 0
    Walk = 1


class Entity(AbstractEntityDrawData):

    def __init__(self, position: pygame.Vector2, hit_box: pygame.Rect, rotation_degrees=0, layer=0):

        super().__init__(position, rotation_degrees)
        self.hit_box: pygame.Rect = hit_box
        self.speed = 100
        self.do_blink = False
        self.do_wobble = False

    def move(self, move_direction: int, dt: float, world):
        move_distance = move_direction * self.speed * dt
        self.position.x += move_distance
        self.anim_state = AnimState.Walk
        if move_direction < 0:
            self.flip_x = True  # WalkDirection.left
        elif move_direction > 0:
            self.flip_x = False  # WalkDirection.right
        else:
            self.anim_state = AnimState.Idle
        # else leave walk_direction as is

        segment = world.get_ground_at(self.position)
        if segment and 0 < self.position.x < SCREENSIZE[0]:
            h = segment.get_height(self.position)
            self.position.y = h
            self.hit_box.midbottom = self.position
            self.rotation = segment.angle
        else:
            self.position.x -= move_distance

    def update(self):
        ticks = pygame.time.get_ticks() / 1000  # seconds
        if self.do_wobble:
            self.scale.x = 1 + 0.3 * math.sin(ticks * 2 * math.pi)
            self.scale.y = 1 + 0.12 * math.cos(ticks * 2 * math.pi)
        if self.do_blink:
            self.alpha = 0.5 + 0.5 * math.sin(ticks / 2 * 2 * math.pi)


class GroundSegment:

    def __init__(self, x_position: float, angle_deg: float):
        self.start = pygame.Vector2(x_position, 0)
        self.angle = -angle_deg  # positive angle should go up -> use negative angle instead to invert y
        self.end = self.start.copy()

    def get_height(self, position):
        if self.angle == 0.0:
            return self.start.y
        else:
            # y = m * x + c   assuming y goes up and x to the right
            m = math.radians(self.angle)
            c = self.start.y
            return m * (position.x - self.start.x) + c


class World:

    def __init__(self):
        self.ground_segments = []

    def append_ground_segment(self, segment):
        if self.ground_segments:
            last = self.ground_segments[-1]
            h = last.get_height(segment.start)
            segment.start.y = h
            last.end.update(segment.start)
        self.ground_segments.append(segment)

    def get_ground_at(self, position):
        for s_ in self.ground_segments:
            if s_.start.x <= position.x < s_.end.x:
                return s_
        return None


def main():
    pygame.init()
    pygame.display.set_caption(
        "sideview demo, transformations, keys: w (wobble), b (blink), s (scale), z (zoom), left, right")
    screen = pygame.display.set_mode(SCREENSIZE)

    world = World()
    initial_segment = GroundSegment(20, 0)
    initial_segment.start.y = 400
    world.append_ground_segment(initial_segment)
    world.append_ground_segment(GroundSegment(50, 0))
    world.append_ground_segment(GroundSegment(100, 0))
    world.append_ground_segment(GroundSegment(150, 0))
    world.append_ground_segment(GroundSegment(200, 15))
    world.append_ground_segment(GroundSegment(220, 30))
    world.append_ground_segment(GroundSegment(240, 45))
    world.append_ground_segment(GroundSegment(300, 45))
    world.append_ground_segment(GroundSegment(320, 30))
    world.append_ground_segment(GroundSegment(340, 15))
    world.append_ground_segment(GroundSegment(360, 0))
    world.append_ground_segment(GroundSegment(400, 0))
    world.append_ground_segment(GroundSegment(420, -15))
    world.append_ground_segment(GroundSegment(440, -30))
    world.append_ground_segment(GroundSegment(460, -45))
    world.append_ground_segment(GroundSegment(500, -30))
    world.append_ground_segment(GroundSegment(520, -15))
    world.append_ground_segment(GroundSegment(540, 0))
    world.append_ground_segment(GroundSegment(780, 0))

    idle_surf = pygame.Surface((30, 90), pygame.SRCALPHA)
    idle_surf.fill((100, 200, 0, 128))
    walk_surf = pygame.Surface((30, 90), pygame.SRCALPHA)
    walk_surf.fill((0, 200, 200, 128))
    bush_surf = pygame.Surface((30, 30), pygame.SRCALPHA)
    bush_surf.fill(pygame.Color("green"))
    walk_sprite_sheet = pygame.image.load("../resources/dude/DudeWalking.png").convert_alpha()
    r = pygame.Rect(0, 0, 28, 49)
    walk_images = []
    for i in range(9):
        surface = pygame.Surface((28, 49), pygame.SRCALPHA)
        surface.blit(walk_sprite_sheet, (0, 0), r.move(i * 28, 0))
        walk_images.append(surface)
    idle_sprite_sheet = pygame.image.load("../resources/dude/DudeLookingUp.png").convert_alpha()
    idle_images = []
    for i in range(5):
        surface = pygame.Surface((28, 49), pygame.SRCALPHA)
        surface.blit(idle_sprite_sheet, (0, 0), r.move(i * 28, 0))
        idle_images.append(surface)

    # create entities and corresponding animations
    player = Entity(pygame.Vector2(20, 0), pygame.Rect(0, 0, 30, 90))
    idle_anim = EntitySprite(player, idle_images, 10, pygame.Vector2(0.0, 25.0), AnimState.Idle)
    walk_anim = EntitySprite(player, walk_images, 10, pygame.Vector2(0.0, 25.0), AnimState.Walk)
    bush = StaticSprite(pygame.Vector2(200, 370), [bush_surf], fps=10, offset=pygame.Vector2(0, 0), layer=-1)

    text_spr = TextSprite(player, pygame.Vector2(0, 50), pygame.font.Font(None, 20), "this is the player")

    sprites = [idle_anim, walk_anim, text_spr, bush]
    sprites_drawer = SpritesDrawer()

    clock = pygame.Clock()
    running = True
    while running:
        dt = clock.tick(60) / 1000.0  # convert to seconds
        # events
        for event in pygame.event.get():

            if event.type == pygame.QUIT:
                running = False
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    running = False
                elif event.key == pygame.K_b:
                    player.do_blink = not player.do_blink
                    if not player.do_blink:
                        player.alpha = 1.0
                elif event.key == pygame.K_w:
                    player.do_wobble = not player.do_wobble
                    if not player.do_wobble:
                        player.scale.update(1.0, 1.0)
                elif event.key == pygame.K_s:
                    if event.mod & pygame.KMOD_SHIFT:
                        # anim scale
                        objs = [idle_anim, walk_anim]
                    else:
                        # entity scale
                        objs = [player]
                    for obj in objs:
                        if obj.scale.x == 1.0:
                            obj.scale.update(obj.scale.x * 3, obj.scale.y * 2)
                        else:
                            obj.scale.update(1.0, 1.0)
                elif event.key == pygame.K_z:
                    if player.zoom == 1.0:
                        player.zoom = 2.0
                    else:
                        player.zoom = 1.0

        pressed = pygame.key.get_pressed()
        r_pressed = pressed[pygame.K_RIGHT]
        l_pressed = pressed[pygame.K_LEFT]

        # update
        player.move((r_pressed - l_pressed), dt, world)
        player.update()

        # draw
        # draw ground
        for idx, s in enumerate(world.ground_segments[:-1]):
            pygame.draw.line(screen, pygame.Color("white"), world.ground_segments[idx].start,
                             world.ground_segments[idx + 1].start)

        # sprites
        sprites_drawer.render_sprites(screen, sprites, dt)

        # debug
        # highlight segment the player currently stands on
        segment = world.get_ground_at(player.position)
        if segment:
            pygame.draw.line(screen, pygame.Color("red"), segment.start, segment.end)
        # player hit box and position (small circle)
        color = pygame.Color("green") if player.anim_state == AnimState.Idle else pygame.Color("yellow")
        pygame.draw.rect(screen, color, player.hit_box, 2)
        pygame.draw.circle(screen, color, player.position, 3)
        factor = -1 if player.flip_x else 1
        pygame.draw.line(screen, color, player.position,
                         player.position + factor * pygame.Vector2(30, 0).rotate(player.rotation), 1)

        # bring it to the screen
        pygame.display.flip()
        screen.fill((255, 0, 255))

    pygame.quit()


if __name__ == '__main__':
    main()
